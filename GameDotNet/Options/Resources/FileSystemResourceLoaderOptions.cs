﻿namespace GameDotNet.Options.Resources
{
	public class FileSystemResourceLoaderOptions
	{
		public string BaseDirectory { get; set; } = "Resources";

		public static implicit operator BaseDirectoryOptions( FileSystemResourceLoaderOptions options )
			=> new BaseDirectoryOptions { BaseDirectory = options.BaseDirectory };

		public static implicit operator FileSystemResourceLoaderOptions( BaseDirectoryOptions options )
			=> new FileSystemResourceLoaderOptions { BaseDirectory = options.BaseDirectory };
	}
}