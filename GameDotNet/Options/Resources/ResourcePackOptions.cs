﻿namespace GameDotNet.Options.Resources
{
	public class ResourcePackOptions
	{
		public string BaseDirectory { get; set; } = "Packs";

		public static implicit operator BaseDirectoryOptions( ResourcePackOptions options )
			=> new BaseDirectoryOptions { BaseDirectory = options.BaseDirectory };

		public static implicit operator ResourcePackOptions( BaseDirectoryOptions options )
			=> new ResourcePackOptions { BaseDirectory = options.BaseDirectory };
	}
}