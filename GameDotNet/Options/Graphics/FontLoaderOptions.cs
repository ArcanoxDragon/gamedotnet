﻿namespace GameDotNet.Options.Graphics
{
	public class FontLoaderOptions
	{
		public string BaseDirectory { get; set; } = "Fonts";

		public static implicit operator BaseDirectoryOptions( FontLoaderOptions options )
			=> new BaseDirectoryOptions { BaseDirectory = options.BaseDirectory };

		public static implicit operator FontLoaderOptions( BaseDirectoryOptions options )
			=> new FontLoaderOptions { BaseDirectory = options.BaseDirectory };
	}
}