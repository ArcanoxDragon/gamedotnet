﻿namespace GameDotNet.Options.Graphics
{
	public class TextureLoaderOptions
	{
		public string BaseDirectory { get; set; } = "Textures";

		public static implicit operator BaseDirectoryOptions( TextureLoaderOptions options )
			=> new BaseDirectoryOptions { BaseDirectory = options.BaseDirectory };

		public static implicit operator TextureLoaderOptions( BaseDirectoryOptions options )
			=> new TextureLoaderOptions { BaseDirectory = options.BaseDirectory };
	}
}