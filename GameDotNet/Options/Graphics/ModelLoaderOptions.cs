﻿namespace GameDotNet.Options.Graphics
{
	public class ModelLoaderOptions
	{
		public string BaseDirectory      { get; set; } = "Models";
		public string MaterialsDirectory { get; set; } = "Materials";

		public static implicit operator BaseDirectoryOptions( ModelLoaderOptions options )
			=> new BaseDirectoryOptions { BaseDirectory = options.BaseDirectory };

		public static implicit operator ModelLoaderOptions( BaseDirectoryOptions options )
			=> new ModelLoaderOptions { BaseDirectory = options.BaseDirectory };
	}
}