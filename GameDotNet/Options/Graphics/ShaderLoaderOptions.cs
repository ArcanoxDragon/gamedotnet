﻿namespace GameDotNet.Options.Graphics
{
	public class ShaderLoaderOptions
	{
		public string BaseDirectory { get; set; } = "Shaders";

		public static implicit operator BaseDirectoryOptions( ShaderLoaderOptions options )
			=> new BaseDirectoryOptions { BaseDirectory = options.BaseDirectory };

		public static implicit operator ShaderLoaderOptions( BaseDirectoryOptions options )
			=> new ShaderLoaderOptions { BaseDirectory = options.BaseDirectory };
	}
}