﻿using GameDotNet.Options.Audio;
using GameDotNet.Options.Graphics;
using GameDotNet.Options.Resources;

namespace GameDotNet.Options
{
	public class AllGameOptions
	{
		public SoundLoaderOptions              Sound              { get; set; } = new SoundLoaderOptions();
		public SoundtrackLoaderOptions         Soundtrack         { get; set; } = new SoundtrackLoaderOptions();
		public FontLoaderOptions               Font               { get; set; } = new FontLoaderOptions();
		public ModelLoaderOptions              Model              { get; set; } = new ModelLoaderOptions();
		public ShaderLoaderOptions             Shader             { get; set; } = new ShaderLoaderOptions();
		public TextureLoaderOptions            Texture            { get; set; } = new TextureLoaderOptions();
		public FileSystemResourceLoaderOptions FileSystemResource { get; set; } = new FileSystemResourceLoaderOptions();
		public ResourcePackOptions             ResourcePack       { get; set; } = new ResourcePackOptions();
		public GameOptions                     Game               { get; set; } = new GameOptions();
	}
}