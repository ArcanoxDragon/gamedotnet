﻿namespace GameDotNet.Options.Audio
{
	public class SoundLoaderOptions
	{
		public string BaseDirectory { get; set; } = "Sounds";

		public static implicit operator BaseDirectoryOptions( SoundLoaderOptions options )
			=> new BaseDirectoryOptions { BaseDirectory = options.BaseDirectory };

		public static implicit operator SoundLoaderOptions( BaseDirectoryOptions options )
			=> new SoundLoaderOptions { BaseDirectory = options.BaseDirectory };
	}
}