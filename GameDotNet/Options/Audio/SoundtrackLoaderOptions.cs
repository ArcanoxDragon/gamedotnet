﻿namespace GameDotNet.Options.Audio
{
	public class SoundtrackLoaderOptions
	{
		public string BaseDirectory { get; set; } = "Music";

		public static implicit operator BaseDirectoryOptions( SoundtrackLoaderOptions options )
			=> new BaseDirectoryOptions { BaseDirectory = options.BaseDirectory };

		public static implicit operator SoundtrackLoaderOptions( BaseDirectoryOptions options )
			=> new SoundtrackLoaderOptions { BaseDirectory = options.BaseDirectory };
	}
}