﻿namespace GameDotNet.Options
{
	public class GameOptions
	{
		public const string DefaultGameName      = "Untitled Game";
		public const int    DefaultWidth         = 800;
		public const int    DefaultHeight        = 600;
		public const int    DefaultShadowmapSize = 4096;
		public const int    DefaultFps           = 60;
		public const int    DefaultTps           = 30;
		public const int    DefaultAntiAliasing  = 8;

		public int    Width            { get; set; } = DefaultWidth;
		public int    Height           { get; set; } = DefaultHeight;
		public int    ShadowMapSize    { get; set; } = DefaultShadowmapSize;
		public int    TargetFps        { get; set; } = DefaultFps;
		public int    TargetTps        { get; set; } = DefaultTps;
		public string GameName         { get; set; } = DefaultGameName;
		public int    AntiAliasSamples { get; set; } = DefaultAntiAliasing;
	}
}