﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace GameDotNet.Audio
{
	public class Soundtrack
	{
		public enum SegmentType
		{
			PlayOnce,
			Loop
		}

		public struct Segment
		{
			public SegmentType Type;
			public Sound       Sound;
			public TimeSpan    Duration;
		}

		private int         currentSegment = 0;
		private SoundSource playSource;
		private Timer       playTimer;

		public Soundtrack()
		{
			this.Segments = new List<Segment>();
		}

		public List<Segment> Segments { get; }

		public void AddSegment( Segment segment )
		{
			this.Segments.Add( segment );
		}

		public void Play( SoundSource source )
		{
			if ( !this.Segments.Any() )
				return;

			this.currentSegment    =  0;
			this.playSource        =  source;
			this.playTimer         =  new Timer( 1 /* ms */ ) { AutoReset = false };
			this.playTimer.Elapsed += this.TimerTicked;
			this.playTimer.Start();
		}

		public void Stop()
		{
			this.playTimer?.Stop();
			this.playSource?.Stop();
			this.playTimer  = null;
			this.playSource = null;
		}

		public void Delete()
		{
			this.Stop();

			foreach ( var segment in this.Segments )
				segment.Sound.Delete();

			this.Segments.Clear();
		}

		private void TimerTicked( object sender, ElapsedEventArgs e )
		{
			var segmentToPlay = this.Segments[ this.currentSegment++ ];

			this.playSource.IsLooping = segmentToPlay.Type == SegmentType.Loop;
			segmentToPlay.Sound.Play( this.playSource );

			if ( !this.playSource.IsLooping && this.currentSegment < this.Segments.Count )
			{
				this.playTimer.Interval = segmentToPlay.Duration.TotalSeconds;
				this.playTimer.Start();
			}
		}
	}
}