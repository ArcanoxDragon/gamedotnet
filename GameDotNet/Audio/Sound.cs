﻿using System;
using GameDotNet.Utility.Audio;
using OpenTK.Audio.OpenAL;

namespace GameDotNet.Audio
{
	public class Sound : IDisposable
	{
		public event Action<Sound> Deleted;

		private int bufferId;

		public Sound( byte[] wavData, ALFormat format, uint sampleRate )
		{
			this.bufferId = AL.GenBuffer();

			AL.BufferData( this.bufferId, format, wavData, wavData.Length, (int) sampleRate );

			// format is ALFormat
			// bytesPerSample is int

			var bytesPerSample = format switch
			{
				ALFormat.Mono8 => 1,
				ALFormat.Stereo8 => 2,
				ALFormat.Mono16 => 2,
				ALFormat.Stereo16 => 4,
				_ => throw new NotSupportedException( $"The audio format {format.ToString()} is not supported" )
			};

			this.Duration = TimeSpan.FromSeconds( wavData.Length / (double) bytesPerSample / sampleRate );
		}

		private Sound()
		{
			this.bufferId = 0;
		}

		~Sound()
		{
			this.Delete();
		}

		public TimeSpan Duration { get; }

		public void Delete()
		{
			if ( this.bufferId > 0 )
				AL.DeleteBuffer( this.bufferId );

			this.bufferId = 0;
			this.Deleted?.Invoke( this );
		}

		public void Play( SoundSource source )
		{
			if ( this.bufferId <= 0 )
				return;

			source.Stop();
			AL.BindBufferToSource( source.SourceID, this.bufferId );

			if ( ALUtility.TryGetError( out var error ) )
				throw new Exception( $"OpenAL Error: {error}" );

			AL.SourcePlay( source.SourceID );

			if ( ALUtility.TryGetError( out error ) )
				throw new Exception( $"OpenAL Error: {error}" );
		}

		public void Dispose()
		{
			this.Delete();
		}
	}
}