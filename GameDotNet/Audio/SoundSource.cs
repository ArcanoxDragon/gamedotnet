﻿using System;
using OpenTK.Audio.OpenAL;

namespace GameDotNet.Audio
{
	public class SoundSource : IDisposable
	{
		private bool  isLooping;
		private float volume;

		public int SourceID { get; private set; }

		public SoundSource()
		{
			this.SourceID = AL.GenSource();
		}

		~SoundSource()
		{
			this.Delete();
		}

		public bool IsLooping
		{
			get => this.isLooping;
			set
			{
				this.isLooping = value;
				AL.Source( this.SourceID, ALSourceb.Looping, value );
			}
		}

		public float Volume
		{
			get => this.volume;
			set
			{
				this.volume = Math.Max( Math.Min( value, 1.0f ), 0.0f );
				AL.Source( this.SourceID, ALSourcef.Gain, this.volume );
			}
		}

		public void Delete()
		{
			this.Stop();

			if ( this.SourceID > 0 )
				AL.DeleteSource( this.SourceID );

			this.SourceID = 0;
		}

		public void Stop()
		{
			AL.SourceStop( this.SourceID );
			AL.BindBufferToSource( this.SourceID, 0 );
		}

		public void Dispose()
		{
			this.Delete();
		}
	}
}