﻿using System;
using GameDotNet.Abstract.Services;
using GameDotNet.Abstract.Services.Graphics;

namespace GameDotNet.Graphics
{
	public enum AnimationDirection
	{
		Forwards,
		Backwards,
	}

	public class TextureAnimation
	{
		private readonly ILogger logger;
		private readonly float   frameHeight;

		private TimeSpan timeOnCurrentFrame;

		public TextureAnimation( ITransformManager transform, ILogger logger, Texture texture )
		{
			if ( texture.Height % texture.Width != 0 )
				throw new ArgumentException( "Texture height must be a multiple of texture width for animation strips", nameof(texture) );

			this.FrameRate  = 30;
			this.Transform  = transform;
			this.Texture    = texture;
			this.FrameCount = texture.Height / texture.Width;

			this.logger      = logger;
			this.frameHeight = 1.0f / this.FrameCount;
		}

		public ITransformManager  Transform    { get; }
		public Texture            Texture      { get; }
		public int                FrameCount   { get; }
		public TimeSpan           FramePeriod  { get; set; }
		public int                CurrentFrame { get; set; }
		public AnimationDirection Direction    { get; set; }

		public double FrameRate
		{
			get => 1.0 / this.FramePeriod.TotalSeconds;
			set => this.FramePeriod = TimeSpan.FromSeconds( 1.0 / value );
		}

		public void Update( TimeSpan deltaTime )
		{
			this.timeOnCurrentFrame += deltaTime;

			if ( this.timeOnCurrentFrame >= this.FramePeriod )
			{
				this.CurrentFrame += this.Direction == AnimationDirection.Forwards ? 1 : -1;

				if ( this.CurrentFrame >= this.FrameCount )
					this.CurrentFrame = 0;
				if ( this.CurrentFrame < 0 )
					this.CurrentFrame = this.FrameCount - 1;

				this.timeOnCurrentFrame = TimeSpan.Zero;
			}
		}

		public void TransformUV()
		{
			var topUV = this.CurrentFrame * this.frameHeight;

			this.Transform.UVScale( 1.0f, this.frameHeight );
			this.Transform.UVTranslate( 0.0f, topUV );
		}
	}
}