﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK.Graphics.OpenGL;
using PixelFormat = System.Drawing.Imaging.PixelFormat;
using GlPixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;

namespace GameDotNet.Graphics
{
	public class Texture
	{
		public event Action<Texture> Deleted;

		public Texture()
		{
			this.TextureId = -1;
			this.Width     = 0;
			this.Height    = 0;
			this.Loaded    = false;
		}

		public Texture( Bitmap bitmap ) : this()
		{
			this.Width     = bitmap.Width;
			this.Height    = bitmap.Height;
			this.TextureId = GL.GenTexture();

			var bitmapData = bitmap.LockBits( new Rectangle( 0, 0, this.Width, this.Height ),
											  ImageLockMode.ReadOnly,
											  PixelFormat.Format32bppArgb );

			GL.BindTexture( TextureTarget.Texture2D, this.TextureId );
			GL.TexImage2D( TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, this.Width, this.Height, 0, GlPixelFormat.Bgra, PixelType.UnsignedByte, bitmapData.Scan0 );
			GL.TexParameter( TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int) TextureMagFilter.Nearest );
			GL.TexParameter( TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int) TextureMinFilter.Nearest );
			GL.BindTexture( TextureTarget.Texture2D, 0 );

			this.Loaded = true;
		}

		public int  TextureId { get; private set; }
		public int  Width     { get; }
		public int  Height    { get; }
		public bool Loaded    { get; }

		public void Bind()
		{
			GL.BindTexture( TextureTarget.Texture2D, this.TextureId );
		}

		public void Unbind()
		{
			GL.BindTexture( TextureTarget.Texture2D, 0 );
		}

		public void Delete()
		{
			if ( this.TextureId < 0 )
				return;

			GL.DeleteTexture( this.TextureId );
			this.TextureId = -1;
			this.Deleted?.Invoke( this );
		}
	}
}