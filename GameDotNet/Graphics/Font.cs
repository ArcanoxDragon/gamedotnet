using System.Collections.Generic;
using System.Drawing;
using GameDotNet.Abstract.Services.Graphics;

namespace GameDotNet.Graphics
{
	public class Font
	{
		private readonly IShapeFactory                shapeFactory;
		private readonly Texture                      texture;
		private readonly Dictionary<char, Rectangle>  characterMetrics;
		private readonly Dictionary<char, RectangleF> uvMetrics;

		public Font( ITransformManager transform,
					 IShapeFactory shapeFactory,
					 Dictionary<char, Rectangle> characterMetrics,
					 Texture texture )
		{
			this.shapeFactory     = shapeFactory;
			this.texture          = texture;
			this.characterMetrics = characterMetrics;
			this.uvMetrics        = this.CalculateUVs();

			this.Transform = transform;
		}

		protected ITransformManager Transform { get; }

		private Dictionary<char, RectangleF> CalculateUVs()
		{
			var bounds = new Dictionary<char, RectangleF>();

			foreach ( var @char in this.characterMetrics.Keys )
			{
				var characterBounds = this.characterMetrics[ @char ];
				var uvX             = characterBounds.X / (float) this.texture.Width;
				var uvY             = characterBounds.Y / (float) this.texture.Height;
				var uvW             = characterBounds.Width / (float) this.texture.Width;
				var uvH             = characterBounds.Height / (float) this.texture.Height;

				bounds[ @char ] = new RectangleF( uvX, uvY, uvW, uvH );
			}

			return bounds;
		}

		public int GetCharWidth( char @char )
		{
			if ( this.characterMetrics.ContainsKey( @char ) )
				return this.characterMetrics[ @char ].Width;

			return 0;
		}

		public int GetCharHeight( char @char )
		{
			if ( this.characterMetrics.ContainsKey( @char ) )
				return this.characterMetrics[ @char ].Height;

			return 0;
		}

		public int GetStringWidth( string str )
		{
			var width = 0;

			foreach ( char c in str )
				width += this.GetCharWidth( c );

			return width;
		}

		public int GetStringHeight( string str )
		{
			if ( str.Length == 0 )
				return this.GetCharHeight( ' ' );

			return this.GetCharHeight( str[ 0 ] );
		}

		public void RenderCharacter( char @char, int x, int y )
		{
			if ( !this.uvMetrics.ContainsKey( @char ) || !this.characterMetrics.ContainsKey( @char ) )
				return;

			this.texture.Bind();

			var charMetrics = this.characterMetrics[ @char ];
			var uvMetrics   = this.uvMetrics[ @char ];

			this.Transform.PushMatrix();
			{
				this.Transform.Scale( charMetrics.Width, charMetrics.Height, 1.0f );
				this.Transform.Translate( x, y, 0 );
				this.Transform.UVPushMatrix();
				{
					this.Transform.UVScale( uvMetrics.Width, uvMetrics.Height );
					this.Transform.UVTranslate( uvMetrics.X, uvMetrics.Y );
					this.shapeFactory.Plane.Render();
				}
				this.Transform.UVPopMatrix();
			}
			this.Transform.PopMatrix();
		}

		public void RenderString( string text, int x, int y )
		{
			var curX = x;
			var curY = y;

			foreach ( var @char in text )
			{
				this.RenderCharacter( @char, curX, curY );
				curX += this.GetCharWidth( @char );
			}
		}
	}
}