using System;
using GameDotNet.Abstract.Services;
using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Exceptions.Graphics;
using OpenTK.Graphics.OpenGL;
using GameDotNet.Utility.Graphics;

namespace GameDotNet.Graphics
{
	public sealed class Shader
	{
		public event Action<Shader> Bound, Unbound;

		public Shader( ITransformManager transform,
					   ILogger logger,
					   string vertexSource,
					   string fragmentSource )
		{
			this.Transform = transform;
			this.ProgramId = GL.CreateProgram();

			if ( this.ProgramId <= 0 )
				throw new ShaderCompileException( $"Can't create shader program: {GL.GetError()}" );

			var vertexId    = GL.CreateShader( ShaderType.VertexShader );
			var fragmentId  = GL.CreateShader( ShaderType.FragmentShader );
			var vertSuccess = ShaderUtil.CompileShader( vertexId,   vertexSource,   logger );
			var fragSuccess = ShaderUtil.CompileShader( fragmentId, fragmentSource, logger );

			if ( vertSuccess && fragSuccess )
			{
				GL.AttachShader( this.ProgramId, vertexId );
				GL.AttachShader( this.ProgramId, fragmentId );
				GL.LinkProgram( this.ProgramId );
				GL.GetProgramInfoLog( this.ProgramId, out var info );

				if ( info.Length > 0 )
				{
					logger.WriteLine( $"[Shader Info] {info}" );
				}

				GL.DeleteShader( vertexId );
				GL.DeleteShader( fragmentId );
			}
			else
			{
				GL.GetProgramInfoLog( this.ProgramId, out var errorLog );

				throw new ShaderCompileException( $"Error compiling shader: {errorLog}" );
			}
		}

		public int ProgramId { get; }

		public bool IsValid => this.ProgramId > 0;

		private ITransformManager Transform { get; }

		public void Bind()
		{
			if ( !this.IsValid )
				throw new InvalidOperationException( "Cannot bind an invalid shader" );

			GL.UseProgram( this.ProgramId );
			this.Transform.ForceReUpload();
			this.Bound?.Invoke( this );
		}

		public void Unbind()
		{
			GL.UseProgram( 0 );
			this.Unbound?.Invoke( this );
		}

		public void Delete()
		{
			if ( this.ProgramId == GL.GetInteger( GetPName.CurrentProgram ) )
				this.Unbind();

			if ( this.ProgramId > 0 )
				GL.DeleteProgram( this.ProgramId );
		}
	}
}