using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Services.Graphics;
using OpenTK;
using OpenTK.Graphics;

namespace GameDotNet.Graphics
{
	public class Light
	{
		private readonly float[] lightData = new float[ LightManager.SizeLightStruct / sizeof( float ) ];

		public Light( ITransformManager transform )
		{
			this.Transform            = transform;
			this.AmbientColor         = new Color4( 0,   0,   0,   255 );
			this.DiffuseColor         = new Color4( 255, 255, 255, 255 );
			this.AttenuationConstant  = 0.01f;
			this.AttenuationLinear    = 0.25f;
			this.AttenuationQuadratic = 0.01f;
			this.Position             = new Vector4( 0, 0, 0, 1 );
		}

		protected ITransformManager Transform { get; }

		public Vector4 Position             { get; set; }
		public Vector3 Target               { get; set; }
		public Color4  DiffuseColor         { get; set; }
		public Color4  AmbientColor         { get; set; }
		public float   AttenuationConstant  { get; set; }
		public float   AttenuationLinear    { get; set; }
		public float   AttenuationQuadratic { get; set; }
		public float   Shadow               { get; set; }

		public void SetTransformForShadowRender( Vector3? eyeOffset = null )
		{
			var offset = eyeOffset ?? Vector3.Zero;

			this.Transform.SetView( Matrix4.LookAt( offset - this.Position.Xyz, Vector3.Zero, Vector3.UnitZ ) );
			this.Transform.SetProjection( Matrix4.CreateOrthographicOffCenter( -20, 20, -20, 20, -20, 40 ) );
			this.Transform.CopyToDepthVP();
		}

		public float[] ToArray()
		{
			var lightPosition = Vector4.Transform( this.Position, this.Transform.Model * this.Transform.View );

			this.lightData[ 0 ]  = lightPosition.X;
			this.lightData[ 1 ]  = lightPosition.Y;
			this.lightData[ 2 ]  = lightPosition.Z;
			this.lightData[ 3 ]  = lightPosition.W;
			this.lightData[ 4 ]  = this.DiffuseColor.R;
			this.lightData[ 5 ]  = this.DiffuseColor.G;
			this.lightData[ 6 ]  = this.DiffuseColor.B;
			this.lightData[ 7 ]  = this.DiffuseColor.A;
			this.lightData[ 8 ]  = this.AmbientColor.R;
			this.lightData[ 9 ]  = this.AmbientColor.G;
			this.lightData[ 10 ] = this.AmbientColor.B;
			this.lightData[ 11 ] = this.AmbientColor.A;
			this.lightData[ 12 ] = this.AttenuationConstant;
			this.lightData[ 13 ] = this.AttenuationLinear;
			this.lightData[ 14 ] = this.AttenuationQuadratic;
			this.lightData[ 15 ] = this.Shadow;

			return this.lightData;
		}

		public override string ToString()
			=> $"Light ({this.DiffuseColor.R:0.00}, {this.DiffuseColor.G:0.00}, {this.DiffuseColor.B:0.00})";

		public virtual Light Clone() => new Light( this.Transform ) {
			DiffuseColor         = this.DiffuseColor,
			AmbientColor         = this.AmbientColor,
			Shadow               = this.Shadow,
			Target               = this.Target,
			Position             = this.Position,
			AttenuationConstant  = this.AttenuationConstant,
			AttenuationLinear    = this.AttenuationLinear,
			AttenuationQuadratic = this.AttenuationQuadratic
		};
	}
}