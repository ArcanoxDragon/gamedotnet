using System;
using OpenTK.Graphics.OpenGL;

namespace GameDotNet.Graphics
{
	public class FrameBuffer
	{
		public FrameBuffer( int width, int height, bool color = false, bool index = false, bool depth = false, bool glow = false )
		{
			this.Complete      = false;
			this.FramebufferId = GL.GenFramebuffer();

			this.Width  = width;
			this.Height = height;

			if ( color )
				this.ColorTextureId = GL.GenTexture();
			else
				this.ColorTextureId = 0;

			if ( index )
				this.IndexTextureId = GL.GenTexture();
			else
				this.IndexTextureId = 0;

			if ( depth )
				this.DepthTextureId = GL.GenTexture();
			else
				this.DepthTextureId = 0;

			if ( glow )
				this.GlowTextureId = GL.GenTexture();
			else
				this.GlowTextureId = 0;
		}

		public int FramebufferId  { get; }
		public int ColorTextureId { get; }
		public int IndexTextureId { get; }
		public int DepthTextureId { get; }
		public int GlowTextureId  { get; }

		public int Width  { get; }
		public int Height { get; }

		public bool Complete { get; protected set; }

		public virtual void Initialize()
		{
			GL.BindFramebuffer( FramebufferTarget.Framebuffer, this.FramebufferId );
			GL.ActiveTexture( TextureUnit.Texture0 );

			if ( this.ColorTextureId > 0 )
			{
				GL.BindTexture( TextureTarget.Texture2DMultisample, this.ColorTextureId );
				GL.TexImage2D( TextureTarget.Texture2DMultisample, 0, PixelInternalFormat.Rgba8, this.Width, this.Height, 0, PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero );
				GL.TexParameter( TextureTarget.Texture2DMultisample, TextureParameterName.TextureMinFilter, (int) TextureMinFilter.Linear );
				GL.TexParameter( TextureTarget.Texture2DMultisample, TextureParameterName.TextureMagFilter, (int) TextureMagFilter.Linear );
				GL.BindTexture( TextureTarget.Texture2DMultisample, 0 );

				GL.FramebufferTexture2D( FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2DMultisample, this.ColorTextureId, 0 );
			}

			if ( this.IndexTextureId > 0 )
			{
				GL.BindTexture( TextureTarget.Texture2DMultisample, this.IndexTextureId );
				GL.TexImage2D( TextureTarget.Texture2DMultisample, 0, PixelInternalFormat.Rgba8, this.Width, this.Height, 0, PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero );
				GL.TexParameter( TextureTarget.Texture2DMultisample, TextureParameterName.TextureMinFilter, (int) TextureMinFilter.Nearest );
				GL.TexParameter( TextureTarget.Texture2DMultisample, TextureParameterName.TextureMagFilter, (int) TextureMagFilter.Nearest );
				GL.BindTexture( TextureTarget.Texture2DMultisample, 0 );

				GL.FramebufferTexture2D( FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment1, TextureTarget.Texture2DMultisample, this.IndexTextureId, 0 );
			}

			if ( this.GlowTextureId > 0 )
			{
				GL.BindTexture( TextureTarget.Texture2DMultisample, this.GlowTextureId );
				GL.TexImage2D( TextureTarget.Texture2DMultisample, 0, PixelInternalFormat.Rgba8, this.Width, this.Height, 0, PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero );
				GL.TexParameter( TextureTarget.Texture2DMultisample, TextureParameterName.TextureMinFilter, (int) TextureMinFilter.Linear );
				GL.TexParameter( TextureTarget.Texture2DMultisample, TextureParameterName.TextureMagFilter, (int) TextureMagFilter.Linear );
				GL.BindTexture( TextureTarget.Texture2DMultisample, 0 );

				GL.FramebufferTexture2D( FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment2, TextureTarget.Texture2DMultisample, this.GlowTextureId, 0 );
			}

			if ( this.DepthTextureId > 0 )
			{
				GL.BindTexture( TextureTarget.Texture2DMultisample, this.DepthTextureId );
				GL.TexImage2D( TextureTarget.Texture2DMultisample, 0, PixelInternalFormat.DepthComponent32, this.Width, this.Height, 0, PixelFormat.DepthComponent, PixelType.Float, IntPtr.Zero );
				GL.TexParameter( TextureTarget.Texture2DMultisample, TextureParameterName.TextureMinFilter,   (int) TextureMinFilter.Nearest );
				GL.TexParameter( TextureTarget.Texture2DMultisample, TextureParameterName.TextureMagFilter,   (int) TextureMagFilter.Nearest );
				GL.TexParameter( TextureTarget.Texture2DMultisample, TextureParameterName.TextureWrapR,       (int) TextureWrapMode.ClampToEdge );
				GL.TexParameter( TextureTarget.Texture2DMultisample, TextureParameterName.TextureWrapS,       (int) TextureWrapMode.ClampToEdge );
				GL.TexParameter( TextureTarget.Texture2DMultisample, TextureParameterName.TextureCompareMode, (int) TextureCompareMode.None );
				GL.BindTexture( TextureTarget.Texture2DMultisample, 0 );

				GL.FramebufferTexture2D( FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, TextureTarget.Texture2DMultisample, this.DepthTextureId, 0 );
			}

			var fboStatus = GL.CheckFramebufferStatus( FramebufferTarget.Framebuffer );

			if ( fboStatus == FramebufferErrorCode.FramebufferComplete || fboStatus == FramebufferErrorCode.FramebufferCompleteExt )
			{
				this.Complete = true;
			}
			else
			{
				throw new InvalidOperationException( $"An error occurred while initializing a framebuffer (status: {fboStatus})" );
			}

			GL.BindFramebuffer( FramebufferTarget.Framebuffer, 0 );
		}

		public virtual void Delete()
		{
			if ( this.ColorTextureId > 0 )
				GL.DeleteTexture( this.ColorTextureId );
			if ( this.IndexTextureId > 0 )
				GL.DeleteTexture( this.IndexTextureId );
			if ( this.DepthTextureId > 0 )
				GL.DeleteTexture( this.DepthTextureId );
			if ( this.GlowTextureId > 0 )
				GL.DeleteTexture( this.GlowTextureId );

			GL.DeleteFramebuffer( this.FramebufferId );
		}

		public virtual void Bind()
		{
			GL.Viewport( 0, 0, this.Width, this.Height );
			GL.BindFramebuffer( FramebufferTarget.Framebuffer, this.FramebufferId );
		}

		public virtual void Unbind()
		{
			GL.BindFramebuffer( FramebufferTarget.Framebuffer, 0 );
		}

		public virtual void BindColorTexture()
		{
			GL.BindTexture( TextureTarget.Texture2DMultisample, this.ColorTextureId );
		}

		public virtual void BindIndexTexture()
		{
			GL.BindTexture( TextureTarget.Texture2DMultisample, this.IndexTextureId );
		}

		public virtual void BindDepthTexture()
		{
			GL.BindTexture( TextureTarget.Texture2DMultisample, this.DepthTextureId );
		}

		public virtual void BindGlowTexture()
		{
			GL.BindTexture( TextureTarget.Texture2DMultisample, this.GlowTextureId );
		}
	}
}