﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using GameDotNet.Abstract.Services.Graphics;

namespace GameDotNet.Graphics
{
	public sealed class TextureLibrary
	{
		private readonly Dictionary<string, Texture> cache;
		private readonly ITextureLoader              textureLoader;

		public TextureLibrary( ITextureLoader textureLoader, string path = "" )
		{
			this.cache         = new Dictionary<string, Texture>();
			this.textureLoader = textureLoader;

			this.BasePath = path;
		}

		public string BasePath { get; }

		public Texture GetTexture( string name )
		{
			if ( !name.ToLower().EndsWith( ".png" ) )
				name += ".png";

			if ( !this.cache.ContainsKey( name ) )
			{
				var textureName = Path.Combine( this.BasePath, name );
				var texture     = this.textureLoader.LoadTexture( textureName );

				texture.Deleted    += this.TextureDeleted;
				this.cache[ name ] =  texture;
			}

			return this.cache[ name ];
		}

		private void TextureDeleted( Texture texture )
		{
			this.UnloadTexture( texture );
		}

		public void UnloadTexture( string name )
		{
			if ( !name.ToLower().EndsWith( ".png" ) )
				name += ".png";

			if ( this.cache.ContainsKey( name ) )
			{
				this.cache[ name ].Delete();
				this.cache.Remove( name );
			}
		}

		public void UnloadTexture( Texture texture )
		{
			if ( this.cache.ContainsValue( texture ) )
			{
				var keysToDelete = this.cache.Keys.Where( k => this.cache[ k ] == texture );

				foreach ( var key in keysToDelete )
					this.cache.Remove( key );

				texture.Delete();
			}
		}

		public void UnloadAllTextures()
		{
			var toDelete = this.cache.Values.ToList();

			this.cache.Clear();

			foreach ( var texture in toDelete )
				texture.Delete();
		}
	}
}