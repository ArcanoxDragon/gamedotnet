﻿using GameDotNet.Abstract.Services.Graphics;
using OpenTK;

namespace GameDotNet.Graphics.Primitives
{
	public static class Plane
	{
		public static Shape GenerateShape( IModelFactory modelFactory )
		{
			var verts = new[] {
				new Vector4( 0.0f, 0.0f, 0.0f, 1.0f ),
				new Vector4( 0.0f, 1.0f, 0.0f, 1.0f ),
				new Vector4( 1.0f, 1.0f, 0.0f, 1.0f ),
				new Vector4( 1.0f, 0.0f, 0.0f, 1.0f )
			};

			var normals = new[] {
				new Vector3( 0.0f, 0.0f, -1.0f ),
				new Vector3( 0.0f, 0.0f, -1.0f ),
				new Vector3( 0.0f, 0.0f, -1.0f ),
				new Vector3( 0.0f, 0.0f, -1.0f )
			};

			var uvs = new[] {
				new Vector2( 0.0f, 0.0f ),
				new Vector2( 0.0f, 1.0f ),
				new Vector2( 1.0f, 1.0f ),
				new Vector2( 1.0f, 0.0f )
			};

			var indices = new uint[] { 0, 1, 2, 0, 2, 3 };

			return new Shape( modelFactory, "plane", verts, normals, uvs, indices );
		}
	}
}