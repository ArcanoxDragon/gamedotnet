﻿using System.Collections.Generic;

namespace GameDotNet.Graphics.Primitives
{
	public static class PrimitiveCache
	{
		private static readonly Dictionary<Primitive, PrimitiveData> Cache = new Dictionary<Primitive, PrimitiveData>();

		public static PrimitiveData GetPrimitiveData( Primitive primitive )
		{
			if ( !Cache.ContainsKey( primitive ) )
				Cache.Add( primitive, primitive.CalculateData() );

			return Cache[ primitive ];
		}
	}
}