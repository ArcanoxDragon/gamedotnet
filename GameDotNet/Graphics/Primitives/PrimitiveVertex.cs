﻿using OpenTK;

namespace GameDotNet.Graphics.Primitives
{
	public struct PrimitiveVertex
	{
		public Vector2 TexCoord;
		public Vector3 Normal;
		public Vector3 Position;

		#region Equality

		public bool Equals( PrimitiveVertex other )
		{
			return this.TexCoord.Equals( other.TexCoord ) && this.Normal.Equals( other.Normal ) && this.Position.Equals( other.Position );
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = this.TexCoord.GetHashCode();
				hashCode = ( hashCode * 397 ) ^ this.Normal.GetHashCode();
				hashCode = ( hashCode * 397 ) ^ this.Position.GetHashCode();
				return hashCode;
			}
		}

		public override bool Equals( object obj )
		{
			if ( !( obj is PrimitiveVertex other ) )
				return false;

			return this.TexCoord.Equals( other.TexCoord ) && this.Normal.Equals( other.Normal ) && this.Position.Equals( other.Position );
		}

		public static bool operator ==( PrimitiveVertex a, PrimitiveVertex b ) => a.Equals( b );

		public static bool operator !=( PrimitiveVertex a, PrimitiveVertex b ) => !( a == b );

		#endregion
	}
}