﻿namespace GameDotNet.Graphics.Primitives
{
	public abstract class Primitive
	{
		public abstract PrimitiveData CalculateData();
	}
}