﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Extensions;
using OpenTK;

namespace GameDotNet.Graphics.Primitives
{
	public class Cone : Primitive
	{
		#region Shape Generation

		public static Shape GenerateShape( IModelFactory modelFactory )
		{
			var cone     = new Cone( 1.0f, 1.0f, 24 );
			var coneData = PrimitiveCache.GetPrimitiveData( cone );
			var verts    = new Vector4[ coneData.Vertices.Length ];
			var normals  = new Vector3[ coneData.Vertices.Length ];
			var uvs      = new Vector2[ coneData.Vertices.Length ];
			var indices  = coneData.Indices.Select( i => (uint) i ).ToArray();

			for ( int i = 0; i < coneData.Vertices.Length; i++ )
			{
				verts[ i ]   = new Vector4( coneData.Vertices[ i ].Position, 1.0f );
				normals[ i ] = new Vector3( coneData.Vertices[ i ].Normal );
				uvs[ i ]     = coneData.Vertices[ i ].TexCoord;
			}

			return new Shape( modelFactory, "cone", verts, normals, uvs, indices );
		}

		#endregion

		private readonly float radius;
		private readonly float height;
		public           int   segmentCount;

		public Cone( float radius, float height, int segmentCount )
		{
			this.radius       = radius;
			this.height       = height;
			this.segmentCount = segmentCount;
		}

		public override PrimitiveData CalculateData()
		{
			var data     = new PrimitiveData();
			var vertices = new List<PrimitiveVertex>();
			var indices  = new List<int>();

			for ( var i = 0; i < this.segmentCount; i++ )
			{
				var angle1   = i / (float) this.segmentCount * MathHelper.TwoPi;
				var angle2   = ( i + 1 ) % this.segmentCount / (float) this.segmentCount * MathHelper.TwoPi;
				var uvAngle1 = MathHelper.Pi + MathHelper.PiOver4 + angle1 / 4.0f;
				var uvAngle2 = MathHelper.Pi + MathHelper.PiOver4 + angle2 / 4.0f;

				var bottom1 = new PrimitiveVertex {
					Position = {
						X = (float) Math.Cos( angle1 ) * this.radius,
						Y = (float) Math.Sin( angle1 ) * this.radius
					}
				};

				bottom1.Normal.X   = bottom1.Position.Normalized().X * ( this.height / this.radius );
				bottom1.Normal.Y   = bottom1.Position.Normalized().Y * ( this.height / this.radius );
				bottom1.Normal.Z   = this.radius / this.height;
				bottom1.TexCoord.X = 0.5f + (float) Math.Cos( uvAngle1 );
				bottom1.TexCoord.Y = (float) -Math.Sin( uvAngle1 );

				var bottom2 = new PrimitiveVertex {
					Position = {
						X = (float) Math.Cos( angle2 ) * this.radius,
						Y = (float) Math.Sin( angle2 ) * this.radius
					}
				};

				bottom2.Normal.X   = bottom2.Position.Normalized().X * ( this.height / this.radius );
				bottom2.Normal.Y   = bottom2.Position.Normalized().Y * ( this.height / this.radius );
				bottom2.Normal.Z   = this.radius / this.height;
				bottom2.TexCoord.X = 0.5f + (float) Math.Cos( uvAngle2 );
				bottom2.TexCoord.Y = (float) -Math.Sin( uvAngle2 );

				var top = new PrimitiveVertex {
					Position = {
						Z = this.height
					},
					Normal = {
						X = bottom1.Normal.X,
						Y = bottom1.Normal.Y,
						Z = bottom1.Normal.Z
					},
					TexCoord = {
						X = 0.5f
					}
				};

				var bottom0 = new PrimitiveVertex {
					Normal = {
						Z = -1.0f
					},
					TexCoord = {
						X = 0.5f
					}
				};

				// First add the side
				var indexTop     = vertices.AddAndReturnIndex( top );
				var indexBottom1 = vertices.AddAndReturnIndex( bottom1 );
				var indexBottom2 = vertices.AddAndReturnIndex( bottom2 );

				indices.Add( indexTop );
				indices.Add( indexBottom1 );
				indices.Add( indexBottom2 );

				// Now modify and add the bottom
				bottom1.Normal = bottom0.Normal;
				bottom2.Normal = bottom0.Normal;

				var indexBottom = vertices.AddAndReturnIndex( bottom0 );
				indexBottom1 = vertices.AddAndReturnIndex( bottom1 );
				indexBottom2 = vertices.AddAndReturnIndex( bottom2 );

				indices.Add( indexBottom );
				indices.Add( indexBottom2 );
				indices.Add( indexBottom1 );
			}

			data.Vertices = vertices.ToArray();
			data.Indices  = indices.ToArray();

			return data;
		}

		#region Equality

		protected bool Equals( Cone other )
			=> this.radius.Equals( other.radius ) &&
			   this.height.Equals( other.height ) &&
			   this.segmentCount == other.segmentCount;

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = this.radius.GetHashCode();
				hashCode = ( hashCode * 397 ) ^ this.height.GetHashCode();
				hashCode = ( hashCode * 397 ) ^ this.segmentCount;
				return hashCode;
			}
		}

		public override bool Equals( object obj )
		{
			if ( !( obj is Cone other ) )
				return false;

			return Math.Abs( this.radius - other.radius ) < float.Epsilon &&
				   Math.Abs( this.height - other.height ) < float.Epsilon &&
				   this.segmentCount == other.segmentCount;
		}

		public static bool operator ==( Cone a, Cone b )
		{
			if ( (object) a == null || (object) b == null )
				return object.ReferenceEquals( a, b );
			return a.Equals( b );
		}

		public static bool operator !=( Cone a, Cone b )
		{
			if ( (object) a == null || (object) b == null )
				return !object.ReferenceEquals( a, b );
			return !( a == b );
		}

		#endregion
	}
}