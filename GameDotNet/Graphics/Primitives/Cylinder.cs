﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Extensions;
using OpenTK;

namespace GameDotNet.Graphics.Primitives
{
	public class Cylinder : Primitive
	{
		#region Shape Generation

		public static Shape GenerateShape( IModelFactory modelFactory )
		{
			var cylinder     = new Cylinder( 1.0f, 1.0f, 24 );
			var cylinderData = PrimitiveCache.GetPrimitiveData( cylinder );
			var verts        = new Vector4[ cylinderData.Vertices.Length ];
			var normals      = new Vector3[ cylinderData.Vertices.Length ];
			var uvs          = new Vector2[ cylinderData.Vertices.Length ];
			var indices      = cylinderData.Indices.Select( i => (uint) i ).ToArray();

			for ( int i = 0; i < cylinderData.Vertices.Length; i++ )
			{
				verts[ i ]   = new Vector4( cylinderData.Vertices[ i ].Position, 1.0f );
				normals[ i ] = new Vector3( cylinderData.Vertices[ i ].Normal );
				uvs[ i ]     = cylinderData.Vertices[ i ].TexCoord;
			}

			return new Shape( modelFactory, "cylinder", verts, normals, uvs, indices );
		}

		#endregion

		private readonly float radius;
		private readonly float height;
		public           int   segmentCount;

		public Cylinder( float radius, float height, int segmentCount )
		{
			this.radius       = radius;
			this.height       = height;
			this.segmentCount = segmentCount;
		}

		public override PrimitiveData CalculateData()
		{
			var data     = new PrimitiveData();
			var vertices = new List<PrimitiveVertex>();
			var indices  = new List<int>();

			for ( int i = 0; i < this.segmentCount; i++ )
			{
				var angle1 = i / (float) this.segmentCount * MathHelper.TwoPi;
				var angle2 = ( i + 1 ) % this.segmentCount / (float) this.segmentCount * MathHelper.TwoPi;

				var topLeft = new PrimitiveVertex {
					Position = {
						X = (float) Math.Cos( angle1 ) * this.radius,
						Y = (float) Math.Sin( angle1 ) * this.radius,
						Z = this.height
					}
				};

				topLeft.Normal.X   = topLeft.Position.Normalized().X;
				topLeft.Normal.Y   = topLeft.Position.Normalized().Y;
				topLeft.Normal.Z   = this.radius / this.height;
				topLeft.TexCoord.X = i / (float) this.segmentCount;

				var topRight = new PrimitiveVertex {
					Position = {
						X = (float) Math.Cos( angle2 ) * this.radius,
						Y = (float) Math.Sin( angle2 ) * this.radius,
						Z = this.height
					}
				};

				topRight.Normal.X   = topRight.Position.Normalized().X;
				topRight.Normal.Y   = topRight.Position.Normalized().Y;
				topRight.Normal.Z   = this.radius / this.height;
				topRight.TexCoord.X = ( i + 1 ) % this.segmentCount / (float) this.segmentCount;

				var botLeft = new PrimitiveVertex {
					Position = {
						X = (float) Math.Cos( angle1 ) * this.radius,
						Y = (float) Math.Sin( angle1 ) * this.radius
					}
				};

				botLeft.Normal.X   = botLeft.Position.Normalized().X;
				botLeft.Normal.Y   = botLeft.Position.Normalized().Y;
				botLeft.Normal.Z   = this.radius / this.height;
				botLeft.TexCoord.X = i / (float) this.segmentCount;
				botLeft.TexCoord.Y = 1.0f;

				var botRight = new PrimitiveVertex {
					Position = {
						X = (float) Math.Cos( angle2 ) * this.radius,
						Y = (float) Math.Sin( angle2 ) * this.radius
					}
				};

				botRight.Normal.X   = botRight.Position.Normalized().X;
				botRight.Normal.Y   = botRight.Position.Normalized().Y;
				botRight.Normal.Z   = this.radius / this.height;
				botRight.TexCoord.X = ( i + 1 ) % this.segmentCount / (float) this.segmentCount;
				botRight.TexCoord.Y = 1.0f;

				var topMid = new PrimitiveVertex {
					Position = {
						Z = this.height
					},
					Normal = {
						Z = 1.0f
					}
				};

				var botMid = new PrimitiveVertex {
					Normal = {
						Z = -1.0f
					}
				};

				var indexTopMid   = vertices.AddAndReturnIndex( topMid );
				var indexTopLeft  = vertices.AddAndReturnIndex( topLeft );
				var indexTopRight = vertices.AddAndReturnIndex( topRight );
				var indexBotMid   = vertices.AddAndReturnIndex( botMid );
				var indexBotLeft  = vertices.AddAndReturnIndex( botLeft );
				var indexBotRight = vertices.AddAndReturnIndex( botRight );

				indices.Add( indexTopLeft );
				indices.Add( indexBotLeft );
				indices.Add( indexTopRight );

				indices.Add( indexTopRight );
				indices.Add( indexBotLeft );
				indices.Add( indexBotRight );

				indices.Add( indexTopMid );
				indices.Add( indexTopLeft );
				indices.Add( indexTopRight );

				indices.Add( indexBotMid );
				indices.Add( indexBotRight );
				indices.Add( indexBotMid );
			}

			data.Vertices = vertices.ToArray();
			data.Indices  = indices.ToArray();

			return data;
		}

		#region Equality

		protected bool Equals( Cylinder other )
		{
			return this.radius.Equals( other.radius ) &&
				   this.height.Equals( other.height ) &&
				   this.segmentCount == other.segmentCount;
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = this.radius.GetHashCode();
				hashCode = ( hashCode * 397 ) ^ this.height.GetHashCode();
				hashCode = ( hashCode * 397 ) ^ this.segmentCount;
				return hashCode;
			}
		}

		public override bool Equals( object obj )
		{
			if ( !( obj is Cylinder other ) )
				return false;

			return Math.Abs( this.radius - other.radius ) < float.Epsilon &&
				   Math.Abs( this.height - other.height ) < float.Epsilon &&
				   this.segmentCount == other.segmentCount;
		}

		public static bool operator ==( Cylinder a, Cylinder b )
		{
			if ( (object) a == null || (object) b == null )
				return ReferenceEquals( a, b );
			return a.Equals( b );
		}

		public static bool operator !=( Cylinder a, Cylinder b )
		{
			if ( (object) a == null || (object) b == null )
				return !ReferenceEquals( a, b );
			return !( a == b );
		}

		#endregion
	}
}