﻿namespace GameDotNet.Graphics.Primitives
{
	public class PrimitiveData
	{
		public PrimitiveVertex[] Vertices { get; set; }
		public int[]             Indices  { get; set; }
	}
}