﻿using GameDotNet.Abstract.Services.Graphics;
using OpenTK;

namespace GameDotNet.Graphics.Primitives
{
	/// <summary>
	/// This provides a collection of static shapes, which are just textureless models.
	/// They may be used to render simple primitives with custom textures applied instead
	/// of loading an entire OBJ model.
	/// </summary>
	public sealed class Shape
	{
		private readonly IModelFactory modelFactory;
		private readonly string        shapeName;
		private readonly Vector4[]     vertices;
		private readonly Vector3[]     normals;
		private readonly Vector2[]     uvs;
		private readonly uint[]        indices;

		private Model cachedModel;

		internal Shape( IModelFactory modelFactory, string shapeName, Vector4[] vertices, Vector3[] normals, Vector2[] uvs, uint[] indices )
		{
			this.modelFactory = modelFactory;
			this.shapeName    = shapeName;
			this.vertices     = vertices;
			this.normals      = normals;
			this.uvs          = uvs;
			this.indices      = indices;
		}

		public void Render( Shader shader = null, int objectIdOverride = -1 )
		{
			if ( this.cachedModel == null )
			{
				var model = this.modelFactory.Create( this.shapeName );

				model.LoadData( this.vertices, this.normals, this.uvs, this.indices, null, null, null );
				this.cachedModel = model;
			}

			this.cachedModel.RenderModel( shader, objectIdOverride: objectIdOverride );
		}
	}
}