﻿using System;
using System.Linq;
using GameDotNet.Abstract.Services.Graphics;
using OpenTK;

namespace GameDotNet.Graphics.Primitives
{
	public class Sphere : Primitive
	{
		#region Shape Generation

		public static Shape GenerateShape( IModelFactory modelFactory )
		{
			var sphere     = new Sphere( 1.0f, 24, 24 );
			var sphereData = PrimitiveCache.GetPrimitiveData( sphere );
			var verts      = new Vector4[ sphereData.Vertices.Length ];
			var normals    = new Vector3[ sphereData.Vertices.Length ];
			var uvs        = new Vector2[ sphereData.Vertices.Length ];
			var indices    = sphereData.Indices.Select( v => (uint) v ).ToArray();

			for ( int i = 0; i < sphereData.Vertices.Length; i++ )
			{
				verts[ i ]   = new Vector4( sphereData.Vertices[ i ].Position, 1.0f );
				normals[ i ] = new Vector3( sphereData.Vertices[ i ].Normal );
				uvs[ i ]     = sphereData.Vertices[ i ].TexCoord;
			}

			return new Shape( modelFactory, "sphere", verts, normals, uvs, indices );
		}

		#endregion

		public float radius;
		public int   segmentCount, ringCount;

		public Sphere( float radius, int segmentCount, int ringCount )
		{
			this.radius       = radius;
			this.segmentCount = segmentCount;
			this.ringCount    = ringCount;
		}

		public override PrimitiveData CalculateData()
		{
			var data = new PrimitiveData {
				Vertices = this.CalculateVertices(),
				Indices  = this.CalculateElements()
			};

			return data;
		}

		private PrimitiveVertex[] CalculateVertices()
		{
			var vertices = new PrimitiveVertex[ this.segmentCount * this.ringCount ];
			var i        = 0;

			for ( var y = 0; y < this.ringCount; y++ )
			{
				var phi = ( y / (double) ( this.ringCount - 1 ) ) * Math.PI;

				for ( var x = 0; x < this.segmentCount; x++ )
				{
					var theta = ( x / (double) ( this.segmentCount - 1 ) ) * 2 * Math.PI;
					var vertex = new Vector3 {
						X = (float) ( this.radius * Math.Sin( phi ) * Math.Cos( theta ) ),
						Y = (float) ( this.radius * Math.Cos( phi ) ),
						Z = (float) ( this.radius * Math.Sin( phi ) * Math.Sin( theta ) ),
					};
					var normal = Vector3.Normalize( vertex );
					var uv = new Vector2 {
						X = (float) ( x / (double) ( this.segmentCount - 1 ) ),
						Y = (float) ( y / (double) ( this.ringCount - 1 ) )
					};

					vertices[ i++ ] = new PrimitiveVertex {
						Position = vertex,
						Normal   = normal,
						TexCoord = uv
					};
				}
			}

			return vertices;
		}

		private int[] CalculateElements()
		{
			var vertexCount = this.segmentCount * this.ringCount;
			var elementData = new int[ vertexCount * 6 ];
			var i           = 0;

			for ( var y = 0; y < this.ringCount - 1; y++ )
			{
				for ( var x = 0; x < this.segmentCount - 1; x++ )
				{
					elementData[ i++ ] = ( y + 0 ) * this.segmentCount + x;
					elementData[ i++ ] = ( y + 1 ) * this.segmentCount + x;
					elementData[ i++ ] = ( y + 1 ) * this.segmentCount + x + 1;

					elementData[ i++ ] = ( y + 1 ) * this.segmentCount + x + 1;
					elementData[ i++ ] = ( y + 0 ) * this.segmentCount + x + 1;
					elementData[ i++ ] = ( y + 0 ) * this.segmentCount + x;
				}
			}

			foreach ( int index in elementData )
				if ( index >= this.segmentCount * this.ringCount )
					throw new IndexOutOfRangeException();

			return elementData;
		}

		#region Equality

		protected bool Equals( Sphere other )
			=> this.radius.Equals( other.radius ) &&
			   this.segmentCount == other.segmentCount &&
			   this.ringCount == other.ringCount;

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = this.radius.GetHashCode();
				hashCode = ( hashCode * 397 ) ^ this.segmentCount;
				hashCode = ( hashCode * 397 ) ^ this.ringCount;
				return hashCode;
			}
		}

		public override bool Equals( object obj )
		{
			if ( !( obj is Sphere other ) )
				return false;

			return Math.Abs( this.radius - other.radius ) < float.Epsilon &&
				   this.segmentCount == other.segmentCount &&
				   this.ringCount == other.ringCount;
		}

		public static bool operator ==( Sphere a, Sphere b )
		{
			if ( (object) a == null || (object) b == null )
				return object.ReferenceEquals( a, b );

			return a.Equals( b );
		}

		public static bool operator !=( Sphere a, Sphere b )
		{
			if ( (object) a == null || (object) b == null )
				return !object.ReferenceEquals( a, b );

			return !( a == b );
		}

		#endregion
	}
}