using System;
using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Constants;
using GameDotNet.Utility.Graphics;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace GameDotNet.Graphics
{
	public class Model
	{
		private readonly IShaderManager    shaderManager;
		private readonly ITransformManager transform;

		private int indexCount;

		public Model( IShaderManager shaderManager, ITransformManager transform, string name )
		{
			this.shaderManager = shaderManager;
			this.transform     = transform;

			this.ModelName = name;

			this.VertexBufferId  = GL.GenBuffer();
			this.ElementBufferId = GL.GenBuffer();

			this.Vertices = new Vector4[ 0 ];
			this.Normals  = new Vector3[ 0 ];
			this.UvCoords = new Vector2[ 0 ];
		}

		public int    VertexBufferId  { get; }
		public int    ElementBufferId { get; }
		public string ModelName       { get; }

		public Vector4[] Vertices    { get; private set; }
		public Vector3[] Normals     { get; private set; }
		public Vector3[] Tangents    { get; private set; }
		public Vector3[] Binormals   { get; private set; }
		public Vector2[] UvCoords    { get; private set; }
		public Texture   DiffuseMap  { get; private set; }
		public Texture   NormalMap   { get; private set; }
		public Texture   SpecularMap { get; private set; }

		public void Delete()
		{
			GL.DeleteBuffer( this.VertexBufferId );
			GL.DeleteBuffer( this.ElementBufferId );

			this.DiffuseMap?.Delete();
			this.NormalMap?.Delete();
			this.SpecularMap?.Delete();
		}

		public void LoadData( Vector4[] vertices, Vector3[] normals, Vector2[] uvCoords, uint[] indices, Texture diffuseMap, Texture normalMap, Texture specMap )
		{
			if ( vertices.Length != normals.Length || vertices.Length != uvCoords.Length || normals.Length != uvCoords.Length )
			{
				throw new ArgumentException( "Model data must contain the same number of vertices, normals, and uv-coordinates!" );
			}

			if ( indices.Length % 3 != 0 )
			{
				throw new ArgumentException( "Model has a non-uniform amount of faces! Each face must contain exactly three vertex indices.", nameof(indices) );
			}

			this.Vertices = (Vector4[]) vertices.Clone();
			this.Normals  = (Vector3[]) normals.Clone();
			this.UvCoords = (Vector2[]) uvCoords.Clone();

			this.Tangents  = new Vector3[ this.Vertices.Length ];
			this.Binormals = new Vector3[ this.Vertices.Length ];

			// Calculate tangents
			for ( uint i = 0; i < indices.Length; i += 3 )
			{
				var position0 = this.Vertices[ indices[ i + 0 ] ].Xyz;
				var position1 = this.Vertices[ indices[ i + 1 ] ].Xyz;
				var position2 = this.Vertices[ indices[ i + 2 ] ].Xyz;

				var uv0 = this.UvCoords[ indices[ i + 0 ] ];
				var uv1 = this.UvCoords[ indices[ i + 1 ] ];
				var uv2 = this.UvCoords[ indices[ i + 2 ] ];

				var edge0 = position1 - position0;
				var edge1 = position2 - position0;

				var deltaU0 = uv1.X - uv0.X;
				var deltaU1 = uv2.X - uv0.X;
				var deltaV0 = uv1.Y - uv0.Y;
				var deltaV1 = uv2.Y - uv0.Y;

				var factor = 1.0f / ( deltaU0 * deltaV1 - deltaU1 * deltaV0 );

				Vector3 tangent, binormal;

				tangent.X = factor * ( deltaV1 * edge0.X - deltaV0 * edge1.X );
				tangent.Y = factor * ( deltaV1 * edge0.Y - deltaV0 * edge1.Y );
				tangent.Z = factor * ( deltaV1 * edge0.Z - deltaV0 * edge1.Z );

				binormal.X = factor * ( -deltaU1 * edge0.X - deltaU0 * edge1.X );
				binormal.Y = factor * ( -deltaU1 * edge0.Y - deltaU0 * edge1.Y );
				binormal.Z = factor * ( -deltaU1 * edge0.Z - deltaU0 * edge1.Z );

				this.Tangents[ indices[ i + 0 ] ] = tangent;
				this.Tangents[ indices[ i + 1 ] ] = tangent;
				this.Tangents[ indices[ i + 2 ] ] = tangent;

				this.Binormals[ indices[ i + 0 ] ] = binormal;
				this.Binormals[ indices[ i + 1 ] ] = binormal;
				this.Binormals[ indices[ i + 2 ] ] = binormal;
			}

			this.DiffuseMap  = diffuseMap;
			this.NormalMap   = normalMap;
			this.SpecularMap = specMap;

			var structData = new float[ vertices.Length * 15 ]; // 4 (pos) + 3 (normal) + 3 (binormal) + 3 (tangent) + 2 (uv)

			this.indexCount = indices.Length;

			var j = 0;
			for ( var i = 0; i < vertices.Length; i++ )
			{
				structData[ j++ ] = vertices[ i ].X;
				structData[ j++ ] = vertices[ i ].Y;
				structData[ j++ ] = vertices[ i ].Z;
				structData[ j++ ] = vertices[ i ].W;

				structData[ j++ ] = this.Tangents[ i ].X;
				structData[ j++ ] = this.Tangents[ i ].Y;
				structData[ j++ ] = this.Tangents[ i ].Z;

				structData[ j++ ] = this.Binormals[ i ].X;
				structData[ j++ ] = this.Binormals[ i ].Y;
				structData[ j++ ] = this.Binormals[ i ].Z;

				structData[ j++ ] = normals[ i ].X;
				structData[ j++ ] = normals[ i ].Y;
				structData[ j++ ] = normals[ i ].Z;

				structData[ j++ ] = uvCoords[ i ].X;
				structData[ j++ ] = uvCoords[ i ].Y;
			}

			GL.BindBuffer( BufferTarget.ArrayBuffer, this.VertexBufferId );
			GL.BufferData( BufferTarget.ArrayBuffer, (IntPtr) ( sizeof( float ) * structData.Length ), structData, BufferUsageHint.StaticDraw );
			GL.BindBuffer( BufferTarget.ArrayBuffer, 0 );

			GL.BindBuffer( BufferTarget.ElementArrayBuffer, this.ElementBufferId );
			GL.BufferData( BufferTarget.ElementArrayBuffer, (IntPtr) ( sizeof( uint ) * indices.Length ), indices, BufferUsageHint.StaticDraw );
			GL.BindBuffer( BufferTarget.ElementArrayBuffer, 0 );
		}

		private void SetupTextures( Shader shader )
		{
			shader.SetUniform( "tex",  0 );
			shader.SetUniform( "bump", 1 );
			shader.SetUniform( "spec", 2 );

			if ( this.DiffuseMap != null && this.DiffuseMap.Loaded )
			{
				GL.ActiveTexture( TextureUnit.Texture0 );
				this.DiffuseMap.Bind();
			}

			if ( this.NormalMap != null && this.NormalMap.Loaded )
			{
				GL.ActiveTexture( TextureUnit.Texture1 );
				shader.SetUniform( "useBump", 1 );
				this.NormalMap.Bind();
			}
			else
			{
				GL.ActiveTexture( TextureUnit.Texture1 );
				shader.SetUniform( "useBump", 0 );
			}

			if ( this.SpecularMap != null && this.SpecularMap.Loaded )
			{
				GL.ActiveTexture( TextureUnit.Texture2 );
				shader.SetUniform( "useSpec", 1 );
				this.SpecularMap.Bind();
			}
			else
			{
				GL.ActiveTexture( TextureUnit.Texture2 );
				shader.SetUniform( "useSpec", 0 );
			}

			GL.ActiveTexture( TextureUnit.Texture0 );
		}

		public void RenderModel( Shader shader = null, bool textures = true, int objectIdOverride = -1 )
		{
			shader??=this.shaderManager.Current;

			if ( this.transform.NeedsReUpload )
				this.transform.SendMatrices( shader );

			if ( textures )
				this.SetupTextures( shader );

			GL.BindBuffer( BufferTarget.ArrayBuffer,        this.VertexBufferId );
			GL.BindBuffer( BufferTarget.ElementArrayBuffer, this.ElementBufferId );

			if ( objectIdOverride < 0 )
				shader.SetUniform( "bufIndex", this.ElementBufferId );
			else
				shader.SetUniform( "bufIndex", objectIdOverride );

			var uVertex   = GL.GetAttribLocation( shader.ProgramId, "vertexPos" );
			var uTangent  = GL.GetAttribLocation( shader.ProgramId, "vertexTangent" );
			var uBinormal = GL.GetAttribLocation( shader.ProgramId, "vertexBinormal" );
			var uNormal   = GL.GetAttribLocation( shader.ProgramId, "vertexNormal" );
			var uUV       = GL.GetAttribLocation( shader.ProgramId, "vertexUV" );

			if ( uVertex >= 0 )
			{
				GL.EnableVertexAttribArray( uVertex );
				GL.VertexAttribPointer( uVertex, 4, VertexAttribPointerType.Float, false, GlSizes.SizeBufferEntry, 0 );
			}
			else
			{
				GL.DisableVertexAttribArray( uVertex );
			}

			if ( uTangent >= 0 )
			{
				GL.EnableVertexAttribArray( uTangent );
				GL.VertexAttribPointer( uTangent, 3, VertexAttribPointerType.Float, false, GlSizes.SizeBufferEntry, GlSizes.SizeVec4 );
			}
			else
			{
				GL.DisableVertexAttribArray( uTangent );
			}

			if ( uBinormal >= 0 )
			{
				GL.EnableVertexAttribArray( uBinormal );
				GL.VertexAttribPointer( uBinormal, 3, VertexAttribPointerType.Float, false, GlSizes.SizeBufferEntry, GlSizes.SizeVec4 + GlSizes.SizeVec3 );
			}
			else
			{
				GL.DisableVertexAttribArray( uBinormal );
			}

			if ( uNormal >= 0 )
			{
				GL.EnableVertexAttribArray( uNormal );
				GL.VertexAttribPointer( uNormal, 3, VertexAttribPointerType.Float, false, GlSizes.SizeBufferEntry, GlSizes.SizeVec4 + GlSizes.SizeVec3 + GlSizes.SizeVec3 );
			}
			else
			{
				GL.DisableVertexAttribArray( uNormal );
			}

			if ( uUV >= 0 )
			{
				GL.EnableVertexAttribArray( uUV );
				GL.VertexAttribPointer( uUV, 2, VertexAttribPointerType.Float, false, GlSizes.SizeBufferEntry, GlSizes.SizeVec4 + GlSizes.SizeVec3 + GlSizes.SizeVec3 + GlSizes.SizeVec3 );
			}
			else
			{
				GL.DisableVertexAttribArray( uUV );
			}

			GL.DrawElements( PrimitiveType.Triangles, this.indexCount, DrawElementsType.UnsignedInt, IntPtr.Zero );

			GL.BindBuffer( BufferTarget.ArrayBuffer,        0 );
			GL.BindBuffer( BufferTarget.ElementArrayBuffer, 0 );
		}
	}
}