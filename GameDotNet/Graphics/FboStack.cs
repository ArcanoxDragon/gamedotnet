using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;

namespace GameDotNet.Graphics
{
	public class FboStack
	{
		private readonly Stack<FrameBuffer> stack;

		private FrameBuffer currentFbo;

		public FboStack()
		{
			this.stack = new Stack<FrameBuffer>();
		}

		public void Push( FrameBuffer fbo )
		{
			if ( this.currentFbo != null )
				this.stack.Push( this.currentFbo );

			fbo.Bind();
			this.currentFbo = fbo;
		}

		public FrameBuffer Pop()
		{
			if ( this.stack.Count > 0 )
			{
				this.currentFbo = this.stack.Pop();
				this.currentFbo.Bind();
			}
			else
			{
				GL.BindFramebuffer( FramebufferTarget.Framebuffer, 0 );
				this.currentFbo = null;
			}

			return this.currentFbo;
		}
	}
}