﻿namespace GameDotNet.Constants
{
	public static class GlSizes
	{
		public const int SizeVec4        = sizeof( float ) * 4;
		public const int SizeVec3        = sizeof( float ) * 3;
		public const int SizeVec2        = sizeof( float ) * 2;
		public const int SizeBufferEntry = SizeVec4 + SizeVec3 + SizeVec3 + SizeVec3 + SizeVec2;
	}
}