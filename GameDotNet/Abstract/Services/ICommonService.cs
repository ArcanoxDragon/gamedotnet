﻿using GameDotNet.Abstract.Services.Audio;
using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Abstract.Services.Resources;

namespace GameDotNet.Abstract.Services
{
	public interface ICommonService
	{
		ILogger                Logger      { get; }
		ISoundManager          Sounds      { get; }
		ISoundtrackManager      Soundtracks { get; }
		IAnimationManager      Animations  { get; }
		IFontManager           Fonts       { get; }
		IGlService             Gl          { get; }
		ILightManager          Lights      { get; }
		IModelManager          Models      { get; }
		IShaderManager         Shaders     { get; }
		IShapeFactory          Shapes      { get; }
		ITextureLibraryManager Textures    { get; }
		ITransformManager      Transform   { get; }
		IResourceProvider      Resources   { get; }
	}
}