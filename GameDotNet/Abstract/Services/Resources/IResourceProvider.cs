﻿using System.IO;

namespace GameDotNet.Abstract.Services.Resources
{
	public interface IResourceProvider
	{
		void UnloadAll();
		bool ResourceExists( string fileName );
		Stream GetResourceStream( string fileName, bool disableCache = false );
	}
}