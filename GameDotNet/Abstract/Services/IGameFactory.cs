﻿using GameDotNet.Abstract.Engine;

namespace GameDotNet.Abstract.Services
{
	public interface IGameFactory
	{
		TGame Create<TGame>( IGameRunner runner ) where TGame : IGame;
	}
}