﻿namespace GameDotNet.Abstract.Services
{
	public interface ILogWriter
	{
		void Write( string text );
		void Write( string format, params object[] args );
		void WriteLine( string text );
		void WriteLine( string format, params object[] args );
	}
}