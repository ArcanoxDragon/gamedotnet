﻿using GameDotNet.Abstract.Engine;
using GameDotNet.Engine;

namespace GameDotNet.Abstract.Services
{
	public interface IGameRunnerFactory
	{
		GameRunner<TGame> Create<TGame>() where TGame : IGame;
	}
}