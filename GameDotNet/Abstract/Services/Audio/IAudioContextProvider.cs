﻿using OpenTK.Audio;

namespace GameDotNet.Abstract.Services.Audio
{
    public interface IAudioContextProvider
    {
        AudioContext Context { get; }
    }
}
