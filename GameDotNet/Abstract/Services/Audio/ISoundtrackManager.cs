﻿using GameDotNet.Audio;

namespace GameDotNet.Abstract.Services.Audio
{
	public interface ISoundtrackManager
	{
		void UnloadAll();
		Soundtrack Get( string name );
	}
}