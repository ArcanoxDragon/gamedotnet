﻿using GameDotNet.Audio;

namespace GameDotNet.Abstract.Services.Audio
{
    public interface ISoundManager
	{
		void UnloadAll();
		Sound Get( string name, double startTime = -1.0, double length = -1.0 );
	}
}
