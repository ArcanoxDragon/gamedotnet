﻿using GameDotNet.Audio;

namespace GameDotNet.Abstract.Services.Audio
{
	public interface ISoundLoader
	{
		Sound LoadSound( string name, double startTime = -1.0, double length = -1.0 );
	}
}