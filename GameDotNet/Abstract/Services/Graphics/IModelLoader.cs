﻿using GameDotNet.Graphics;

namespace GameDotNet.Abstract.Services.Graphics
{
	public interface IModelLoader
	{
		Model Load( string name, string groupName = null );
	}
}