﻿using GameDotNet.Graphics;

namespace GameDotNet.Abstract.Services.Graphics
{
	public interface IModelFactory
	{
		Model Create( string name );
	}
}