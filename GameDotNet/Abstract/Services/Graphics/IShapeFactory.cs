﻿using GameDotNet.Graphics.Primitives;

namespace GameDotNet.Abstract.Services.Graphics
{
	public interface IShapeFactory
	{
		Shape Plane    { get; }
		Shape Sphere   { get; }
		Shape Cone     { get; }
		Shape Cylinder { get; }
	}
}