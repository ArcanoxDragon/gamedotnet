﻿using GameDotNet.Graphics;

namespace GameDotNet.Abstract.Services.Graphics
{
	public interface ILightManager
	{
		Light CreateLight();
		void ApplyLights( ref Light[] lights );
		void BindLightBuffer( Shader shader = null );
	}
}