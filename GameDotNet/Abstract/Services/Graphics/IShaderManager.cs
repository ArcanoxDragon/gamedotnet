﻿using GameDotNet.Graphics;

namespace GameDotNet.Abstract.Services.Graphics
{
	public interface IShaderManager : IShaderLoader
	{
		Shader Current { get; }
		void UnloadShader( string vertexName, string fragmentName = default );
		void UnloadShader( Shader shader );
		void UnloadAll();
	}
}