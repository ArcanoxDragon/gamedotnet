using GameDotNet.Graphics;
using OpenTK;

namespace GameDotNet.Abstract.Services.Graphics
{
	public interface ITransformManager
	{
		Matrix4 Projection    { get; }
		Matrix4 View          { get; }
		Matrix4 Model         { get; }
		Matrix4 UV            { get; }
		Matrix4 DepthVP       { get; }
		bool    NeedsReUpload { get; }

		/// <summary>
		/// Forces a re-upload to the graphics card (e.g. if the shader changes)
		/// </summary>
		void ForceReUpload();

		void SetProjection( Matrix4 projection );
		void SetView( Matrix4 view );
		void SetModel( Matrix4 model );
		void SetUV( Matrix4 uv );
		void SendMatrices( Shader shader );
		void SetProjection( float aspect, float fov, float near, float far );
		void SetOrthographic( float left, float right, float bottom, float top, float near, float far );
		void SetCamera( Vector3 eye, Vector3 target, Vector3 up );
		void LoadIdentity();
		void PushMatrix();
		void PopMatrix();
		void Translate( Vector3 amount );
		void Translate( float x, float y, float z );
		void Rotate( float angle, Vector3 axis );
		void Rotate( float angle, float x, float y, float z );
		void Scale( Vector3 scale );
		void Scale( float x, float y, float z );
		void UVLoadIdentity();
		void UVPushMatrix();
		void UVPopMatrix();
		void UVTranslate( Vector2 amount );
		void UVTranslate( float x, float y );
		void UVScale( Vector2 scale );
		void UVScale( float x, float y );
		void CopyToDepthVP();
	}
}