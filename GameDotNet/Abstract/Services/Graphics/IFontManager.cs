﻿using GameDotNet.Graphics;

namespace GameDotNet.Abstract.Services.Graphics
{
	public interface IFontManager
	{
		void UnloadAll();
		Font Get( string name );
	}
}