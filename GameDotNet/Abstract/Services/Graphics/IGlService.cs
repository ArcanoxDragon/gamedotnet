﻿namespace GameDotNet.Abstract.Services.Graphics
{
	public interface IGlService
	{
		ITransformManager Transform    { get; }
		IShapeFactory     ShapeFactory { get; }
		void RenderPlane( int x, int y, int width, int height );

		/// <summary>
		/// Uses a texture that defines border and fill to render a tiled plane.
		/// Texture width and height should be divisible by 3.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <param name="borderSize"></param>
		void RenderBorderedPlane( int x, int y, int width, int height, int borderSize );
	}
}