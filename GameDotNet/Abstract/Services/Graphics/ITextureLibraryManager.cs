﻿using GameDotNet.Graphics;

namespace GameDotNet.Abstract.Services.Graphics
{
	public interface ITextureLibraryManager
	{
		void UnloadAll();
		TextureLibrary GetLibrary( string path = "" );
	}
}