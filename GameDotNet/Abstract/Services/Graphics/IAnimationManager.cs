﻿using System;
using GameDotNet.Graphics;

namespace GameDotNet.Abstract.Services.Graphics
{
	public interface IAnimationManager
	{
		void UnloadAll();
		void UpdateAnimations( TimeSpan deltaTime );
		TextureAnimation GetOrCreate( Texture texture, string name = "" );
	}
}