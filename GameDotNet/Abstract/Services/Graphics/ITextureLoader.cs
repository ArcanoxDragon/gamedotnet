﻿using GameDotNet.Graphics;

namespace GameDotNet.Abstract.Services.Graphics
{
	public interface ITextureLoader
	{
		Texture LoadTexture( string name );
	}
}