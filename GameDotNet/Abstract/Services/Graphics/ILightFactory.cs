﻿using GameDotNet.Graphics;

namespace GameDotNet.Abstract.Services.Graphics
{
	public interface ILightFactory
	{
		Light Create();
	}
}