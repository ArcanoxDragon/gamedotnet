﻿using GameDotNet.Graphics;

namespace GameDotNet.Abstract.Services.Graphics
{
	public interface IModelManager
	{
		void UnloadModel( string path, string groupName = default );
		void UnloadModel( Model model );
		void UnloadAll();
		Model Get( string name, string groupName = default );
	}
}