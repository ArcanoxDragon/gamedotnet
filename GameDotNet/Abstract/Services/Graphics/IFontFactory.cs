﻿using System.Collections.Generic;
using System.Drawing;
using GameDotNet.Graphics;
using Font = GameDotNet.Graphics.Font;

namespace GameDotNet.Abstract.Services.Graphics
{
	public interface IFontFactory
	{
		Font Create( Dictionary<char, Rectangle> characterMetrics, Texture texture );
	}
}