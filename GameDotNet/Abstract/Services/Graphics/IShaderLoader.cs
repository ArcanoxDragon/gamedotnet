﻿using GameDotNet.Graphics;

namespace GameDotNet.Abstract.Services.Graphics
{
	public interface IShaderLoader
	{
		Shader Get( string vertexName, string fragmentName = default );
	}
}