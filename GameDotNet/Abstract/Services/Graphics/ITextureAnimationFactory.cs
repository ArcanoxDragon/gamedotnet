﻿using GameDotNet.Graphics;

namespace GameDotNet.Abstract.Services.Graphics
{
	public interface ITextureAnimationFactory
	{
		TextureAnimation Create( Texture texture );
	}
}