﻿using System;
using GameDotNet.Abstract.Services;
using GameDotNet.Abstract.Services.Audio;
using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Abstract.Services.Resources;
using OpenTK;

namespace GameDotNet.Abstract.Engine
{
	public abstract class GameBase : IGame
	{
		protected GameBase( ICommonService common, IGameRunner runner )
		{
			this.Common = common;
			this.Runner = runner;
		}

		public ICommonService Common { get; }
		public IGameRunner    Runner { get; }
		public GameWindow     Window => this.Runner.Window;

		public virtual void PreInitialize() { }
		public virtual void Initialize() { }
		public virtual void PostInitialize() { }

		public virtual void OnClose() { }

		public abstract void Update( TimeSpan deltaTime );
		public abstract void Render( TimeSpan deltaTime );

		#region ICommonService

		public ILogger                Logger      => this.Common.Logger;
		public ISoundManager          Sounds      => this.Common.Sounds;
		public ISoundtrackManager      Soundtracks => this.Common.Soundtracks;
		public IAnimationManager      Animations  => this.Common.Animations;
		public IFontManager           Fonts       => this.Common.Fonts;
		public IGlService             Gl          => this.Common.Gl;
		public ILightManager          Lights      => this.Common.Lights;
		public IModelManager          Models      => this.Common.Models;
		public IShaderManager         Shaders     => this.Common.Shaders;
		public IShapeFactory          Shapes      => this.Common.Shapes;
		public ITextureLibraryManager Textures    => this.Common.Textures;
		public ITransformManager      Transform   => this.Common.Transform;
		public IResourceProvider      Resources   => this.Common.Resources;

		#endregion
	}
}