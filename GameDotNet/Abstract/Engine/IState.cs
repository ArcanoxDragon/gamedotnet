﻿using System;

namespace GameDotNet.Abstract.Engine
{
	public interface IState<in TGame, out TState>
		where TGame : IGameWithStates<TGame, TState>
		where TState : IState<TGame, TState>
	{
		void Update( TimeSpan deltaTime );
		void Render( TimeSpan deltaTime );
		void Enter();
		void Leave();
	}
}