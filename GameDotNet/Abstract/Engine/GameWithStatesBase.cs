﻿using System;
using System.Threading;
using GameDotNet.Abstract.Services;

namespace GameDotNet.Abstract.Engine
{
	public abstract class GameWithStatesBase<TGame, TState> : GameBase, IGameWithStates<TGame, TState>
		where TGame : IGameWithStates<TGame, TState>
		where TState : IState<TGame, TState>
	{
		private readonly IStateFactory stateFactory;
		private readonly Mutex         updateMutex = new Mutex();
		private readonly Mutex         renderMutex = new Mutex();

		protected GameWithStatesBase(
			ICommonService common,
			IGameRunner runner,
			IStateFactory stateFactory
		) : base( common, runner )
		{
			this.stateFactory = stateFactory;
		}

		public TState CurrentState { get; private set; }

		/// <inheritdoc />
		public void SwitchState<T>() where T : TState
		{
			// Dispatch the actual state switch to a new thread so that we can return to the
			// calling thread. This lets the tick loop perform state changes.
			new Thread( this.SwitchState_Do<T> ).Start();
		}

		public sealed override void Update( TimeSpan deltaTime )
		{
			this.updateMutex.WaitOne();

			this.PreUpdate( deltaTime );

			if ( this.CurrentState == null )
				this.EmptyStateUpdate( deltaTime );
			else
				this.CurrentState.Update( deltaTime );

			this.PostUpdate( deltaTime );

			this.updateMutex.ReleaseMutex();
		}

		protected virtual void PreUpdate( TimeSpan deltaTime ) { }
		protected virtual void EmptyStateUpdate( TimeSpan deltaTime ) { }
		protected virtual void PostUpdate( TimeSpan deltaTime ) { }

		public sealed override void Render( TimeSpan deltaTime )
		{
			this.renderMutex.WaitOne();

			this.PreRender( deltaTime );

			if ( this.CurrentState == null )
				this.EmptyStateRender( deltaTime );
			else
				this.CurrentState.Render( deltaTime );

			this.PostRender( deltaTime );

			this.renderMutex.ReleaseMutex();
		}

		public override void OnClose()
		{
			this.CurrentState?.Leave();
			this.CurrentState = default;

			base.OnClose();
		}

		protected virtual void PreRender( TimeSpan deltaTime ) { }
		protected virtual void EmptyStateRender( TimeSpan deltaTime ) { }
		protected virtual void PostRender( TimeSpan deltaTime ) { }

		private void SwitchState_Do<T>() where T : TState
		{
			// Wait on outer runner mutexes
			this.Runner.Synchronized( () => {
				// Wait on our inner mutexes
				this.updateMutex.WaitOne();
				this.renderMutex.WaitOne();

				// It's now safe to leave the state and enter the new one using a Very Fun™ factory method
				this.CurrentState?.Leave();
				this.CurrentState = this.stateFactory.Create<T, TGame, TState>( (TGame) (object) this /* yuck, bivariance */ );
				this.CurrentState.Enter();

				// Release inner mutexes
				this.renderMutex.ReleaseMutex();
				this.updateMutex.ReleaseMutex();
			} );
		}
	}
}