﻿namespace GameDotNet.Abstract.Engine
{
	public interface IStateFactory
	{
		TState Create<TState, TGame, TStateBase>( TGame game )
			where TState : TStateBase
			where TGame : IGameWithStates<TGame, TStateBase>
			where TStateBase : IState<TGame, TStateBase>;
	}
}