﻿using System;
using GameDotNet.Abstract.Services;
using GameDotNet.Options;
using OpenTK;

namespace GameDotNet.Abstract.Engine
{
	public interface IGameRunner
	{
		string      Title           { get; set; }
		GameOptions Options         { get; }
		ILogger     Logger          { get; }
		GameWindow  Window          { get; }
		TimeSpan    LastTickPeriod  { get; }
		double      TicksPerSecond  { get; }
		TimeSpan    LastFramePeriod { get; }
		double      FramesPerSecond { get; }
		bool        IsRunning       { get; }
		ulong       CurrentTick     { get; }
		ulong       CurrentFrame    { get; }

		void Run();
		void TickSynchronized( Action action );
		void FrameSynchronized( Action action );
		void Synchronized( Action action );
		void Exit();
	}
}