﻿using System;

namespace GameDotNet.Abstract.Engine
{
	public abstract class State<TGame, TState> : IState<TGame, TState>
		where TGame : IGameWithStates<TGame, TState>
		where TState : IState<TGame, TState>
	{
		protected State( TGame game )
		{
			this.Game = game;
		}

		public TGame Game { get; }

		public void Enter()
		{
			this.OnEntering();
			this.OnEntered();
		}

		public void Leave()
		{
			this.OnLeaving();
			this.OnLeft();
		}

		protected virtual void OnEntering() { }
		protected virtual void OnEntered() { }
		protected virtual void OnLeaving() { }
		protected virtual void OnLeft() { }

		public abstract void Update( TimeSpan deltaTime );
		public abstract void Render( TimeSpan deltaTime );
	}
}