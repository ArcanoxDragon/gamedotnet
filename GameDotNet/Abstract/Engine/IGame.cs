﻿using System;
using GameDotNet.Abstract.Services;
using OpenTK;

namespace GameDotNet.Abstract.Engine
{
	public interface IGame : ICommonService
	{
		IGameRunner Runner { get; }
		GameWindow  Window { get; }

		void PreInitialize();
		void Initialize();
		void PostInitialize();

		void OnClose();

		void Update( TimeSpan deltaTime );
		void Render( TimeSpan deltaTime );
	}
}