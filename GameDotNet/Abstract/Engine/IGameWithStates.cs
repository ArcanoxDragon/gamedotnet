﻿namespace GameDotNet.Abstract.Engine
{
	public interface IGameWithStates<out TGame, TState> : IGame
		where TGame : IGameWithStates<TGame, TState>
		where TState : IState<TGame, TState>
	{
		TState CurrentState { get; }

		/// <summary>
		/// Switches the game state to a new state of the specified type
		/// </summary>
		/// <typeparam name="T">The type of state to switch to (instantiation is done by the game)</typeparam>
		void SwitchState<T>() where T : TState;
	}
}