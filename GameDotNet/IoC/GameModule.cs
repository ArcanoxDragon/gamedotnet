﻿using GameDotNet.Abstract.Engine;
using GameDotNet.Abstract.Services;
using GameDotNet.Abstract.Services.Audio;
using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Graphics;
using GameDotNet.Services;
using GameDotNet.Services.Audio;
using GameDotNet.Services.Graphics;
using Ninject.Extensions.Factory;
using Ninject.Modules;
using ObjLoader.Loader.Loaders;

namespace GameDotNet.IoC
{
	public class GameModule : NinjectModule
	{
		public override void Load()
		{
			// General
			this.Bind<ICommonService>().To<CommonService>();
			this.Bind<ILogger>().To<Logger>();
			this.Bind<ILogWriter>().To<ConsoleLogWriter>();
			this.Bind<IGameRunnerFactory>().ToFactory();
			this.Bind<IGameFactory>().ToFactory();
			this.Bind<IStateFactory>().ToFactory();

			// Audio
			this.Bind<IAudioContextProvider>().To<DefaultAudioContextProvider>().InSingletonScope();
			this.Bind<ISoundLoader>().To<WavSoundLoader>().Named( "Wav" );
			this.Bind<ISoundManager>().To<SoundManager>().InSingletonScope();
			this.Bind<ISoundtrackManager>().To<SoundtrackManager>().InSingletonScope();

			// Graphics
			this.Bind<IGlService>().To<GlService>().InSingletonScope();
			this.Bind<ITextureAnimationFactory>().ToFactory();
			this.Bind<IAnimationManager>().To<AnimationManager>().InSingletonScope();
			this.Bind<ITextureLoader>().To<DefaultTextureLoader>().InSingletonScope();
			this.Bind<ITextureLibraryManager>().To<TextureLibraryManager>().InSingletonScope();
			this.Bind<FboStack>().ToSelf().InSingletonScope();
			this.Bind<IFontFactory>().ToFactory();
			this.Bind<IFontManager>().To<FontManager>().InSingletonScope();
			this.Bind<ILightFactory>().ToFactory();
			this.Bind<ILightManager>().To<LightManager>().InSingletonScope();
			this.Bind<IMaterialStreamProvider>().To<DefaultMaterialStreamProvider>();
			this.Bind<IModelLoader>().To<ObjModelLoader>().Named( "Obj" );
			this.Bind<IModelFactory>().ToFactory();
			this.Bind<IModelManager>().To<ModelManager>().InSingletonScope();
			this.Bind<IShaderManager, IShaderLoader>().To<ShaderManager>().InSingletonScope();
			this.Bind<IShapeFactory>().To<DefaultShapeFactory>().InSingletonScope();
			this.Bind<ITransformManager>().To<TransformManager>().InSingletonScope();
		}
	}
}