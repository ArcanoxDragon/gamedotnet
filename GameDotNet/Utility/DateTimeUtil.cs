﻿using System;

namespace GameDotNet.Utility
{
	public static class DateTimeUtil
	{
		public static readonly  TimeSpan OneSecond = new TimeSpan( 0, 0, 1 );
		private static readonly DateTime Epoch     = new DateTime( 1970, 1, 1 );

		public static long MillisSinceEpoch()
		{
			return (long) ( DateTime.UtcNow - Epoch ).TotalMilliseconds;
		}
	}
}