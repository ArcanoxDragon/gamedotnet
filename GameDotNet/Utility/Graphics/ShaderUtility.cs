using GameDotNet.Abstract.Services;
using GameDotNet.Graphics;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace GameDotNet.Utility.Graphics
{
	public static class ShaderUtil
	{
		public static bool CompileShader( int shaderID, string shaderSource, ILogger logger )
		{
			GL.ShaderSource( shaderID, shaderSource );
			GL.CompileShader( shaderID );

			GL.GetShaderInfoLog( shaderID, out var info );

			if ( info.Length > 0 )
				logger.WriteLine( info );

			GL.GetShader( shaderID, ShaderParameter.CompileStatus, out var compileResult );

			if ( compileResult != 1 )
				return false;

			return true;
		}

		public static void SetUniform( this Shader shader, string name, int value )
		{
			if ( shader == null ) return;

			var loc = GL.GetUniformLocation( shader.ProgramId, name );

			GL.Uniform1( loc, value );
		}

		public static void SetUniform( this Shader shader, string name, int[] value )
		{
			if ( shader == null ) return;

			var loc = GL.GetUniformLocation( shader.ProgramId, name );

			GL.Uniform1( loc, value.Length, value );
		}

		public static void SetUniform( this Shader shader, string name, float value )
		{
			if ( shader == null ) return;

			var loc = GL.GetUniformLocation( shader.ProgramId, name );

			GL.Uniform1( loc, value );
		}

		public static void SetUniform( this Shader shader, string name, Color4 color )
		{
			if ( shader == null ) return;

			var loc = GL.GetUniformLocation( shader.ProgramId, name );

			GL.Uniform4( loc, color );
		}

		public static void SetUniform( this Shader shader, string name, Matrix4 matrix )
		{
			if ( shader == null ) return;

			var loc = GL.GetUniformLocation( shader.ProgramId, name );

			GL.UniformMatrix4( loc, false, ref matrix );
		}

		public static void SetUniform( this Shader shader, string name, Matrix3 matrix )
		{
			if ( shader == null ) return;

			var loc = GL.GetUniformLocation( shader.ProgramId, name );

			GL.UniformMatrix3( loc, false, ref matrix );
		}

		public static void SetUniform( this Shader shader, string name, Vector4 vector )
		{
			if ( shader == null ) return;

			var loc = GL.GetUniformLocation( shader.ProgramId, name );

			GL.Uniform4( loc, vector );
		}

		public static void SetUniform( this Shader shader, string name, Vector3 vector )
		{
			if ( shader == null ) return;

			var loc = GL.GetUniformLocation( shader.ProgramId, name );

			GL.Uniform3( loc, vector );
		}

		public static void SetUniform( this Shader shader, string name, Vector2 vector )
		{
			if ( shader == null ) return;

			var loc = GL.GetUniformLocation( shader.ProgramId, name );

			GL.Uniform2( loc, vector );
		}
	}
}