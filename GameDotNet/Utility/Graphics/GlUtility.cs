﻿using System.Drawing;
using OpenTK.Graphics.OpenGL;

namespace GameDotNet.Utility.Graphics
{
	public static class GlUtility
	{
		public static void InitializeOpenGL( Rectangle viewport )
		{
			GL.Viewport( viewport );
			GL.Enable( EnableCap.Multisample );
			GL.Enable( EnableCap.Blend );
			GL.Disable( EnableCap.CullFace );
			GL.DepthFunc( DepthFunction.Less );
			GL.BlendFunc( BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha );
		}
	}
}