﻿using OpenTK.Audio.OpenAL;

namespace GameDotNet.Utility.Audio
{
    public static class ALUtility
    {
		public static bool TryGetError( out string errorMessage )
		{
			var error = AL.GetError();

			if ( error == ALError.NoError )
			{
				errorMessage = default;
				return false;
			}

			errorMessage = AL.GetErrorString( error );
			return true;
		}
    }
}
