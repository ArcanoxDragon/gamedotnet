﻿using System.IO;
using System.Reflection;

namespace GameDotNet.Utility
{
    public static class FileUtilities
    {
		public static string GetTempDirectory()
		{
			var assemblyFullName = Assembly.GetEntryAssembly().FullName;
			var tempFolderName = $"{assemblyFullName.Replace( '.', '_' )}-temp";
			var fullTempFolderPath = Path.Combine( Path.GetTempPath(), tempFolderName );

			if ( !Directory.Exists( fullTempFolderPath ) )
				Directory.CreateDirectory( fullTempFolderPath );

			return fullTempFolderPath;
		}
    }
}
