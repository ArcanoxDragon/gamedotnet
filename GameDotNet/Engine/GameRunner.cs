﻿using System;
using System.Threading;
using GameDotNet.Abstract.Engine;
using GameDotNet.Abstract.Services;
using GameDotNet.Options;
using GameDotNet.Utility.Graphics;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace GameDotNet.Engine
{
	public class GameRunner<TGame> : IGameRunner where TGame : IGame
	{
		public event Action<Exception> OnException;

		private bool shouldRun;

		public GameRunner( GameOptions options, ILogger logger, IGameFactory gameFactory )
		{
			this.Options = options;
			this.Logger  = logger;

			this.InitializeGameWindow();

			this.Game = gameFactory.Create<TGame>( this );
		}

		#region Properties

		public GameOptions Options { get; }
		public ILogger     Logger  { get; }

		public string     Title           { get; set; } = "";
		public TGame      Game            { get; }
		public GameWindow Window          { get; private set; }
		public TimeSpan   LastTickPeriod  { get; private set; }
		public double     TicksPerSecond  { get; private set; }
		public TimeSpan   LastFramePeriod { get; private set; }
		public double     FramesPerSecond { get; private set; }
		public bool       IsRunning       { get; private set; }
		public ulong      CurrentTick     { get; private set; }
		public ulong      CurrentFrame    { get; private set; }
		public Thread     RenderThread    { get; private set; }
		public Thread     UpdateThread    { get; private set; }
		public Mutex      UpdateMutex     { get; } = new Mutex();
		public Mutex      RenderMutex     { get; } = new Mutex();

		#endregion Properties

		private void InitializeGameWindow()
		{
			var graphicsMode = new GraphicsMode( new ColorFormat( DisplayDevice.Default.BitsPerPixel ), 32, 0, this.Options.AntiAliasSamples );

			// === Set up game window ===
			this.Logger.WriteLine( "Creating game window" );
			this.Window         =  new GameWindow( this.Options.Width, this.Options.Height, graphicsMode ) { VSync = VSyncMode.Off };
			this.Window.Closing += this.OnWindowClosing;
			this.Window.Resize  += this.OnResize;

			// Grab the context
			this.Window.MakeCurrent();

			// Initialize OpenGL on the window
			this.InitializeOpenGL();

			// Release the context
			this.Window.Context.MakeCurrent( null );
		}

		private void InitializeOpenGL()
		{
			// === Initialize OpenGL ===
			this.Logger.WriteLine( "Initializing OpenGL subsystem" );

			GlUtility.InitializeOpenGL( this.Window.ClientRectangle );
		}

		public void Run()
		{
			this.Game.PreInitialize();

			// === Initialize game ===
			this.Logger.WriteLine( "Initializing game" );
			this.Game.Initialize();

			// === Set up threads ===
			this.Logger.WriteLine( "Initializing worker threads" );
			this.UpdateThread = new Thread( this.DoTickThread ) { Name   = $"{this.Options.GameName} Tick", };
			this.RenderThread = new Thread( this.DoRenderThread ) { Name = $"{this.Options.GameName} Render", };

			// === Start threads ===
			this.Logger.WriteLine( "Starting worker threads" );
			this.shouldRun = true;
			this.UpdateThread.Start( this.Options.TargetTps );
			this.RenderThread.Start( this.Options.TargetFps );

			this.Game.PostInitialize();
			this.Logger.WriteLine( "Game initialized, displaying main window" );

			// === Finalize initialization ===

			this.Window.Visible = true;
			this.IsRunning      = true;
			this.CurrentTick    = 0;

			this.ProcessEvents();
			this.ShutdownGame();
		}

		private void ProcessEvents()
		{
			while ( this.UpdateThread?.IsAlive == true || this.RenderThread?.IsAlive == true )
			{
				this.Window.ProcessEvents();
				Thread.Sleep( 100 );
				this.Window.Title = this.Title;

				if ( this.Window.IsExiting )
					this.Exit();
			}
		}

		public void TickSynchronized( Action action )
		{
			try
			{
				this.UpdateMutex.WaitOne();
				action();
			}
			finally
			{
				this.UpdateMutex.ReleaseMutex();
			}
		}

		public void FrameSynchronized( Action action )
		{
			try
			{
				this.RenderMutex.WaitOne();
				action();
			}
			finally
			{
				this.RenderMutex.ReleaseMutex();
			}
		}

		public void Synchronized( Action action )
		{
			this.TickSynchronized( () => this.FrameSynchronized( action ) );
		}

		public void Exit()
		{
			new Thread( this.Do_Exit ).Start();
		}

		private void ShutdownGame()
		{
			if ( !this.IsRunning )
				throw new InvalidOperationException( "Cannot shut down game that is not running" );

			this.Game.OnClose();
			this.Window.Exit();
			this.IsRunning = false;
		}

		#region Threads

		private void DoTickThread( object param )
		{
			if ( !( param is int targetTickRate ) )
				return;

			var      targetTimePerTick     = TimeSpan.FromSeconds( 1.0 / targetTickRate );
			var      averageTickPeriod     = TimeSpan.Zero;
			var      averageCount          = 0;
			var      lastCalculatedAverage = DateTime.UtcNow;
			var      tickSleepMutex        = new ManualResetEvent( false );
			DateTime tickStart;
			TimeSpan tickDuration;
			TimeSpan timeToSleep;

			while ( this.shouldRun )
			{
#if !DEBUG
				try
#endif
				{
					tickStart = DateTime.UtcNow;
					this.UpdateMutex.WaitOne();
					this.Game.Update( this.LastTickPeriod );
					this.UpdateMutex.ReleaseMutex();

					tickDuration = DateTime.UtcNow - tickStart;
					timeToSleep  = targetTimePerTick - tickDuration;

					if ( timeToSleep > TimeSpan.Zero )
						tickSleepMutex.WaitOne( timeToSleep );

					this.CurrentTick++;

					// Can't use frameDuration because this needs to include the sleep time
					this.LastTickPeriod =  DateTime.UtcNow - tickStart;
					averageTickPeriod   += this.LastTickPeriod;
					averageCount++;

					if ( ( DateTime.UtcNow - lastCalculatedAverage ).TotalSeconds >= 1 )
					{
						this.TicksPerSecond   = averageCount / averageTickPeriod.TotalSeconds;
						averageCount          = 0;
						averageTickPeriod     = TimeSpan.Zero;
						lastCalculatedAverage = DateTime.UtcNow;
					}
				}
#if !DEBUG
				catch ( Exception ex )
				{
					this.OnException?.Invoke( ex );
					this.Exit();
				}
#endif
			}

			this.Logger.WriteLine( "Tick thread shutting down" );
		}

		private void DoRenderThread( object param )
		{
			if ( !( param is int targetFrameRate ) )
				return;

			// Transfer control of OpenGL context to this thread
			this.Window.MakeCurrent();

			var      targetTimePerFrame    = TimeSpan.FromSeconds( 1.0 / targetFrameRate );
			var      averageFramePeriod    = TimeSpan.Zero;
			var      averageCount          = 0;
			var      lastCalculatedAverage = DateTime.UtcNow;
			var      frameSleepMutex       = new ManualResetEvent( false );
			DateTime frameStart;
			TimeSpan frameDuration;
			TimeSpan timeToSleep;

			while ( this.shouldRun )
			{
#if !DEBUG
				try
#endif
				{
					frameStart = DateTime.UtcNow;
					this.RenderMutex.WaitOne();
					this.Game.Render( this.LastFramePeriod );
					this.Window.SwapBuffers();
					this.RenderMutex.ReleaseMutex();

					if ( targetFrameRate > 0 )
					{
						frameDuration = DateTime.UtcNow - frameStart;
						timeToSleep   = targetTimePerFrame - frameDuration;

						if ( timeToSleep > TimeSpan.Zero )
							frameSleepMutex.WaitOne( timeToSleep );
					}

					this.CurrentFrame++;

					// Can't use frameDuration because this needs to include the sleep time
					this.LastFramePeriod =  DateTime.UtcNow - frameStart;
					averageFramePeriod   += this.LastFramePeriod;
					averageCount++;

					if ( ( DateTime.UtcNow - lastCalculatedAverage ).TotalSeconds > 1 )
					{
						this.FramesPerSecond  = averageCount / averageFramePeriod.TotalSeconds;
						averageCount          = 0;
						averageFramePeriod    = TimeSpan.Zero;
						lastCalculatedAverage = DateTime.UtcNow;
					}
				}
#if !DEBUG
				catch ( Exception ex )
				{
					this.OnException?.Invoke( ex );
					this.Exit();
				}
#endif
			}

			this.Logger.WriteLine( "Render thread shutting down" );
		}

		#endregion Threads

		protected void OnResize( object sender, EventArgs e )
		{
			GL.Viewport( 0, 0, this.Window.Width, this.Window.Height );
		}

		protected virtual void OnWindowClosing( object sender, System.ComponentModel.CancelEventArgs e )
		{
			e.Cancel = true;
			this.Exit();
		}

		private void Do_Exit()
		{
			// Signal to threads that we should quit
			this.shouldRun = false;

			var waitStart   = DateTime.UtcNow;
			var maxWaitTime = TimeSpan.FromSeconds( 10 );

			// Wait for the update thread to quit
			var updateThreadQuit = this.UpdateThread.Join( maxWaitTime );

			// Subtract the time it took for update to exit from our allowed wait time
			maxWaitTime -= DateTime.UtcNow - waitStart;

			if ( maxWaitTime < TimeSpan.Zero )
				maxWaitTime = TimeSpan.Zero;

			// Wait for the render thread to quit
			var renderThreadQuit = this.RenderThread.Join( maxWaitTime );

			if ( !updateThreadQuit || !renderThreadQuit )
			{
				this.Logger.WriteLine( "Game appears to be frozen..." );
			}
		}
	}
}