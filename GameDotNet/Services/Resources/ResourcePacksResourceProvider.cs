﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using GameDotNet.Abstract.Services;
using GameDotNet.Abstract.Services.Resources;
using GameDotNet.Extensions;
using GameDotNet.Options.Resources;
using GameDotNet.Resources;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace GameDotNet.Services.Resources
{
	public class ResourcePacksResourceProvider : IResourceProvider
	{
		private static readonly JsonSerializerSettings JsonSettings = new JsonSerializerSettings {
			ContractResolver = new CamelCasePropertyNamesContractResolver(),
		};

		private readonly Dictionary<string, ResourcePack> loadedPacks;
		private readonly ResourcePackOptions              options;
		private readonly ILogger                          logger;

		private List<ResourcePack> sortedCache;

		public ResourcePacksResourceProvider( ResourcePackOptions options,
											  ILogger logger )
		{
			this.loadedPacks = new Dictionary<string, ResourcePack>();
			this.options     = options;
			this.logger      = logger;

			this.FindAndLoadPacks();
		}

		public void UnloadAll()
		{
			foreach ( var pack in this.loadedPacks.Values )
			{
				pack.UncacheAll();
			}

			this.loadedPacks.Clear();
		}

		public bool ResourceExists( string fileName ) => this.sortedCache.Any( pack => pack.ContainsFile( fileName ) );

		public Stream GetResourceStream( string fileName, bool disableCache = false )
		{
			var firstFound = this.sortedCache.FirstOrDefault( pack => pack.ContainsFile( fileName ) );

			if ( firstFound == default )
				throw new FileNotFoundException( $"Resource not found in any loaded resource packs: {fileName}" );

			return firstFound.OpenFile( fileName );
		}

		public void ReloadPacks()
		{
			this.logger.WriteLine( "Reloading all resource packs..." );

			this.loadedPacks.Clear();
			this.FindAndLoadPacks();
		}

		private void FindAndLoadPacks()
		{
			var packsDirectory = Path.GetFullPath( this.options.BaseDirectory );

			this.logger.WriteLine( $"Looking for resource packs in {packsDirectory}" );

			if ( !Directory.Exists( packsDirectory ) )
			{
				this.logger.WriteLine( "No resource packs found" );
				return;
			}

			foreach ( var packFile in Directory.EnumerateFiles( packsDirectory, "*.zip" ) )
			{
				this.logger.WriteLine( $"Located resource pack candidate {Path.GetFileName( packFile )}" );

				using ( var archive = ZipFile.OpenRead( packFile ) )
				{
					var manifestEntry = archive.GetEntry( "manifest.json" );

					if ( manifestEntry == default )
					{
						this.logger.WriteLine( "\tmanifest.json file not found. Skipping this resource pack." );
						continue;
					}

					var manifestJson = manifestEntry.ReadAllText();
					var manifest     = JsonConvert.DeserializeObject<ResourcePackManifest>( manifestJson, JsonSettings );

					if ( manifest.Id.IsNullOrEmpty() )
					{
						this.logger.WriteLine( "\tmanifest.json is missing the \"id\" property. Skipping this resource pack." );
						continue;
					}

					if ( manifest.Name.IsNullOrEmpty() )
					{
						this.logger.WriteLine( "\tmanifest.json is missing the \"name\" property. Skipping this resource pack." );
						continue;
					}

					if ( this.loadedPacks.ContainsKey( manifest.Id ) )
					{
						this.logger.WriteLine( $"\tA duplicate resource pack ID \"{manifest.Id}\" was found. Duplicates will not be loaded." );
						continue;
					}

					var resourcePack = new ResourcePack( manifest, archive );

					this.logger.WriteLine( $"\tFinished loading resource pack \"{resourcePack.Name}\" with ID \"{resourcePack.Id}\"." );
					this.loadedPacks[ resourcePack.Name ] = resourcePack;
				}
			}

			this.sortedCache = this.loadedPacks.Values
								   .OrderByDescending( pack => pack.Layer )
								   .ThenBy( pack => pack.Id )
								   .ToList();
		}
	}
}