﻿using System.IO;
using GameDotNet.Abstract.Services.Resources;
using GameDotNet.Options.Resources;

namespace GameDotNet.Services.Resources
{
	public class FileSystemResourceProvider : IResourceProvider
	{
		private readonly FileSystemResourceLoaderOptions options;

		public FileSystemResourceProvider( FileSystemResourceLoaderOptions options )
		{
			this.options = options;
		}

		public void UnloadAll() { }

		public string GetFullPath( string fileName )
			=> Path.GetFullPath( Path.Combine( this.options.BaseDirectory, fileName ) );

		public bool ResourceExists( string fileName )
			=> File.Exists( this.GetFullPath( fileName ) );

		public Stream GetResourceStream( string fileName, bool disableCache = false )
			=> File.Open( this.GetFullPath( fileName ), FileMode.Open, FileAccess.Read, FileShare.Read );
	}
}