﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using GameDotNet.Abstract.Services.Resources;

namespace GameDotNet.Services.Resources
{
	public class CompoundResourceProvider : IResourceProvider
	{
		private readonly List<IResourceProvider> childProviders;

		public CompoundResourceProvider()
		{
			this.childProviders = new List<IResourceProvider>();
		}

		public CompoundResourceProvider( params IResourceProvider[] childProviders )
		{
			this.childProviders = new List<IResourceProvider>( childProviders );
		}

		public void UnloadAll()
		{
			foreach ( var provider in this.childProviders )
				provider.UnloadAll();
		}

		public void AddProvider( IResourceProvider resourceProvider )
			=> this.childProviders.Add( resourceProvider );

		public bool ResourceExists( string fileName )
			=> this.childProviders.Any( p => p.ResourceExists( fileName ) );

		public Stream GetResourceStream( string fileName, bool disableCache = false )
		{
			var firstFound = this.childProviders.FirstOrDefault( p => p.ResourceExists( fileName ) );

			if ( firstFound == default )
				throw new FileNotFoundException( $"Resource not found in any registered providers: {fileName}" );

			return firstFound.GetResourceStream( fileName, disableCache );
		}
	}
}