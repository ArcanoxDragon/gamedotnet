﻿using System;
using GameDotNet.Abstract.Services;

namespace GameDotNet.Services
{
	public class ConsoleLogWriter : ILogWriter
	{
		public void Write( string text ) => Console.Write( text );
		public void Write( string format, params object[] args ) => Console.Write( format, args );
		public void WriteLine( string text ) => Console.WriteLine( text );
		public void WriteLine( string format, params object[] args ) => Console.WriteLine( format, args );
	}
}