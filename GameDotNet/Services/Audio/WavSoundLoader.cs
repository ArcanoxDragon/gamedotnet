﻿using System.IO;
using System.Text;
using GameDotNet.Abstract.Services.Audio;
using GameDotNet.Abstract.Services.Resources;
using GameDotNet.Audio;
using GameDotNet.Exceptions.Audio;
using GameDotNet.Extensions;
using GameDotNet.Options.Audio;
using GameDotNet.Utility.Audio;
using OpenTK.Audio.OpenAL;

namespace GameDotNet.Services.Audio
{
	public class WavSoundLoader : ISoundLoader
	{
		private const uint PcmFormatSize = 16;

		private readonly SoundLoaderOptions options;
		private readonly IResourceProvider  resourceProvider;

		public WavSoundLoader( SoundLoaderOptions options,
							   IResourceProvider resourceProvider )
		{
			this.options          = options;
			this.resourceProvider = resourceProvider;
		}

		public Sound LoadSound( string name, double startTime = -1.0, double length = -1.0 )
		{
			var fileName = Path.Combine( this.options.BaseDirectory, name );

			if ( !this.resourceProvider.ResourceExists( fileName ) )
				throw new WaveLoadException( $"WAV resource does not exist: {fileName}" );

			using ( var stream = this.resourceProvider.GetResourceStream( fileName ) )
			using ( var reader = new BinaryReader( stream ) )
			{
				if ( stream.GetAvailable() < 12 )
					throw new WaveLoadException( "Unexpected end of file while reading WAV header" );

				// Read header (main chunk)
				var fileHeader  = Encoding.ASCII.GetString( reader.ReadBytes( 4 ) );
				var chunkSize   = reader.ReadUInt32() - 4; // Length includes the chunk header
				var chunkHeader = Encoding.ASCII.GetString( reader.ReadBytes( 4 ) );

				if ( fileHeader != "RIFF" || chunkHeader != "WAVE" )
					throw new WaveLoadException( "Not a valid WAV file" );

				if ( stream.GetAvailable() < chunkSize )
					throw new WaveLoadException( "Unexpected end of file while reading WAV data" );

				if ( stream.GetAvailable() < 8 )
					throw new WaveLoadException( "Unexpected end of file while reading PCM header" );

				// Read first sub-chunk (format data)
				var pcmHeader  = Encoding.ASCII.GetString( reader.ReadBytes( 4 ) );
				var formatSize = reader.ReadUInt32();

				if ( pcmHeader != "fmt " || formatSize != PcmFormatSize )
					throw new WaveLoadException( "Invalid PCM header" );

				if ( stream.GetAvailable() < formatSize )
					throw new WaveLoadException( "Unexpected end of file while reading PCM header" );

				// ReSharper disable UnusedVariable
				var format         = reader.ReadUInt16();
				var numChannels    = reader.ReadUInt16();
				var sampleRate     = reader.ReadUInt32();
				var byteRate       = reader.ReadUInt32();
				var blockAlign     = reader.ReadUInt16();
				var bitsPerSample  = reader.ReadUInt16();
				var bytesPerSample = bitsPerSample / 8;
				var alFormat       = (ALFormat) 0;
				// ReSharper restore UnusedVariable

				if ( numChannels == 1 )
				{
					if ( bitsPerSample == 8 )
						alFormat = ALFormat.Mono8;
					else if ( bitsPerSample == 16 )
						alFormat = ALFormat.Mono16;
				}
				else if ( numChannels == 2 )
				{
					if ( bitsPerSample == 8 )
						alFormat = ALFormat.Stereo8;
					else if ( bitsPerSample == 16 )
						alFormat = ALFormat.Stereo16;
				}

				if ( alFormat == 0 )
					throw new WaveLoadException( "Unrecognized audio format" );

				if ( stream.GetAvailable() < 12 )
					throw new WaveLoadException( "Unexpected end of file while reading data header" );

				var dataHeader = Encoding.ASCII.GetString( reader.ReadBytes( 4 ) );

				if ( dataHeader != "data" )
					throw new WaveLoadException( "Invalid WAV data" );

				var dataSize = reader.ReadUInt32();

				if ( stream.GetAvailable() < dataSize )
					throw new WaveLoadException( "Unexpected end of file while reading WAV data" );

				var numSamples = dataSize / ( numChannels * bytesPerSample );
				var songLength = numSamples / (double) sampleRate;

				if ( startTime >= 0.0 && startTime < songLength )
				{
					var startSample = (int) ( startTime * sampleRate );
					var startByte   = startSample * bytesPerSample * numChannels;

					// Skip the required number of bytes and subtract the skipped time from the length
					stream.Seek( startByte, SeekOrigin.Current );
					songLength -= startTime;
				}

				if ( length >= 0.0 && length <= songLength )
				{
					songLength = length;
				}

				var songSamples = (int) ( songLength * sampleRate );
				var songBytes   = songSamples * bytesPerSample * numChannels;
				var songData    = reader.ReadBytes( songBytes );
				var sound       = new Sound( songData, alFormat, sampleRate );

				if ( ALUtility.TryGetError( out var error ) )
					throw new WaveLoadException( $"OpenAL Error: {error}" );

				return sound;
			}
		}
	}
}