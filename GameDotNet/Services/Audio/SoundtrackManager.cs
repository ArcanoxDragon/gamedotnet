﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using GameDotNet.Abstract.Services.Audio;
using GameDotNet.Abstract.Services.Resources;
using GameDotNet.Audio;
using GameDotNet.Exceptions.Audio;
using GameDotNet.Options;
using GameDotNet.Options.Audio;

namespace GameDotNet.Services.Audio
{
	public class SoundtrackManager : ISoundtrackManager
	{
		private readonly Dictionary<string, Soundtrack> cache;

		private readonly SoundtrackLoaderOptions options;
		private readonly IResourceProvider       resourceProvider;
		private readonly ISoundLoader            soundLoader;

		public SoundtrackManager( SoundtrackLoaderOptions options, IResourceProvider resourceProvider )
		{
			this.cache = new Dictionary<string, Soundtrack>();

			this.options          = options;
			this.resourceProvider = resourceProvider;

			// Can't inject because we have to specify a custom options object
			this.soundLoader = new WavSoundLoader( (BaseDirectoryOptions) options, this.resourceProvider );
		}

		public void UnloadAll()
		{
			foreach ( var soundtrack in this.cache.Values)
				soundtrack.Delete();

			this.cache.Clear();
		}

		public Soundtrack Get( string name )
		{
			if ( this.cache.ContainsKey( name ) )
				return this.cache[ name ];

			var soundtrackPath = Path.Combine( this.options.BaseDirectory, name );
			var xmlFileName    = Path.Combine( soundtrackPath,             "Song.xml" );
			var wavFileName    = Path.Combine( soundtrackPath,             "Song.wav" );

			if ( !this.resourceProvider.ResourceExists( xmlFileName ) &&
				 !this.resourceProvider.ResourceExists( wavFileName ) )
				throw new SoundtrackLoadException( $"Soundtrack not found: \"{name}\"" );
			if ( !this.resourceProvider.ResourceExists( xmlFileName ) )
				throw new SoundtrackLoadException( $"Soundtrack \"{name}\" is missing XML file \"Song.xml\"" );
			if ( !this.resourceProvider.ResourceExists( wavFileName ) )
				throw new SoundtrackLoadException( $"Soundtrack \"{name}\" is missing WAV file \"Song.wav\"" );

			using ( var xmlStream = this.resourceProvider.GetResourceStream( xmlFileName ) )
			{
				var document    = XDocument.Load( xmlStream );
				var rootElement = document.Element( "song" );

				if ( rootElement == null )
					throw new SoundtrackLoadException( $"Song.xml for soundtrack \"{name}\" is not a valid XML document" );

				var soundtrack   = new Soundtrack();
				var segmentCount = 0;

				foreach ( var element in rootElement.Elements() )
				{
					var segmentType = element.Name.ToString().ToLower();

					var type = segmentType switch
								   {
								   "once" => Soundtrack.SegmentType.PlayOnce,
								   "loop" => Soundtrack.SegmentType.Loop,
								   _ => throw new SoundtrackLoadException( $"Invalid segment type {segmentType} encountered in Song.xml file for soundtrack {name}" )
								   };
					var startAttribute  = element.Attribute( "start" );
					var lengthAttribute = element.Attribute( "length" );

					if ( startAttribute == null )
						throw new SoundtrackLoadException( $"Segment {segmentCount} in soundtrack {name} is missing the start attribute" );

					if ( !double.TryParse( startAttribute.Value, out var start ) )
						throw new SoundtrackLoadException( $"Value \"{startAttribute.Value}\" is invalid for start attribute of segment {segmentCount} in soundtrack {name}" );

					if ( !double.TryParse( lengthAttribute?.Value, out var length ) )
						length = -1.0;

					var sound = this.soundLoader.LoadSound( $"{name}/Song.wav", start, length );

					if ( sound.Duration > TimeSpan.Zero )
					{
						var segment = new Soundtrack.Segment {
							Duration = sound.Duration,
							Sound    = sound,
							Type     = type
						};

						soundtrack.AddSegment( segment );
					}

					segmentCount++;
				}

				this.cache[ name ] = soundtrack;
				return soundtrack;
			}
		}
	}
}