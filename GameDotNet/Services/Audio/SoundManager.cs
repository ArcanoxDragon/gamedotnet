﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GameDotNet.Abstract.Services.Audio;
using GameDotNet.Audio;
using Ninject;

namespace GameDotNet.Services.Audio
{
	public class SoundManager : ISoundManager
	{
		private readonly Dictionary<(string, double, double), Sound> cache = new Dictionary<(string, double, double), Sound>();

		private readonly ISoundLoader wavSoundLoader;

		public SoundManager( [Named( "Wav" )] ISoundLoader wavSoundLoader )
		{
			this.wavSoundLoader = wavSoundLoader;
		}

		public void UnloadAll()
		{
			var toDelete = this.cache.Values.ToList();

			foreach ( var sound in toDelete )
				sound.Delete();

			this.cache.Clear();
		}

		public Sound Get( string name, double startTime = -1, double length = -1 )
		{
			var cacheKey = (name, startTime, length);

			if ( this.cache.ContainsKey( cacheKey ) )
				return this.cache[ cacheKey ];

			var extension = Path.GetExtension( name )?.ToLower();
			var loader = extension switch {
				".wav" => this.wavSoundLoader,
				_ => throw new NotSupportedException( $"The sound format \"{extension}\" is not supported" )
			};
			var sound = loader.LoadSound( name, startTime, length );

			sound.Deleted += this.SoundDeleted;
			this.cache.Add( cacheKey, sound );

			return sound;
		}

		private void SoundDeleted( Sound sound )
		{
			var deleteKeys = this.cache.Keys.Where( k => this.cache[ k ] == sound );

			foreach ( var key in deleteKeys )
				this.cache.Remove( key );
		}
	}
}