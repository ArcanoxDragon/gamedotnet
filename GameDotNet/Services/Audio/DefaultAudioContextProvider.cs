﻿using GameDotNet.Abstract.Services.Audio;
using OpenTK.Audio;

namespace GameDotNet.Services.Audio
{
	public class DefaultAudioContextProvider : IAudioContextProvider
	{
		public DefaultAudioContextProvider( string deviceName )
		{
			this.Context = new AudioContext( deviceName );
		}

		public DefaultAudioContextProvider() : this( AudioContext.DefaultDevice ) { }

		public AudioContext Context { get; }
	}
}