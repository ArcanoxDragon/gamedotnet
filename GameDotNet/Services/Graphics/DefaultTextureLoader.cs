﻿using System.Drawing;
using System.IO;
using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Abstract.Services.Resources;
using GameDotNet.Graphics;
using GameDotNet.Options.Graphics;

namespace GameDotNet.Services.Graphics
{
	public class DefaultTextureLoader : ITextureLoader
	{
		private readonly TextureLoaderOptions options;
		private readonly IResourceProvider    resourceProvider;

		public DefaultTextureLoader( TextureLoaderOptions options,
									 IResourceProvider resourceProvider )
		{
			this.options          = options;
			this.resourceProvider = resourceProvider;
		}

		public Texture LoadTexture( string name )
		{
			var fileName = Path.Combine( this.options.BaseDirectory, name );

			if ( !this.resourceProvider.ResourceExists( fileName ) )
				throw new FileNotFoundException( $"Texture resource not found: {name}" );

			using ( var textureStream = this.resourceProvider.GetResourceStream( fileName ) )
			using ( var bitmap = new Bitmap( textureStream ) )
			{
				return new Texture( bitmap );
			}
		}
	}
}