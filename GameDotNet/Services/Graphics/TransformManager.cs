using System.Collections.Generic;
using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Graphics;
using GameDotNet.Utility.Graphics;
using OpenTK;

namespace GameDotNet.Services.Graphics
{
	public class TransformManager : ITransformManager
	{
		public static readonly Matrix4 ShadowBiasMatrix = new Matrix4(
			new Vector4( 0.5f, 0.0f, 0.0f, 0.0f ),
			new Vector4( 0.0f, 0.5f, 0.0f, 0.0f ),
			new Vector4( 0.0f, 0.0f, 0.5f, 0.0f ),
			new Vector4( 0.5f, 0.5f, 0.5f, 1.0f ) );

		public Matrix4 Projection    { get; private set; }
		public Matrix4 View          { get; private set; }
		public Matrix4 Model         { get; private set; }
		public Matrix4 UV            { get; private set; }
		public Matrix4 DepthVP       { get; private set; }
		public bool    NeedsReUpload { get; private set; }

		private readonly Stack<Matrix4> modelTransformStack;
		private readonly Stack<Matrix4> uvTransformStack;

		public TransformManager()
		{
			this.Projection = Matrix4.Identity;
			this.View       = Matrix4.Identity;
			this.Model      = Matrix4.Identity;
			this.UV         = Matrix4.Identity;

			this.NeedsReUpload = true;

			this.modelTransformStack = new Stack<Matrix4>();
			this.uvTransformStack    = new Stack<Matrix4>();

			this.PushMatrix();
		}

		#region General

		/// <inheritdoc />
		public void ForceReUpload()
		{
			this.NeedsReUpload = true;
		}

		public void SetProjection( Matrix4 projection )
		{
			this.Projection    = projection;
			this.NeedsReUpload = true;
		}

		public void SetView( Matrix4 view )
		{
			this.View          = view;
			this.NeedsReUpload = true;
		}

		public void SetModel( Matrix4 model )
		{
			this.Model         = model;
			this.NeedsReUpload = true;
		}

		public void SetUV( Matrix4 uv )
		{
			this.UV            = uv;
			this.NeedsReUpload = true;
		}

		public void SendMatrices( Shader shader )
		{
			var modelView = this.Model * this.View;

			shader.SetUniform( "transform",           this.Model );
			shader.SetUniform( "modelView",           modelView );
			shader.SetUniform( "projection",          this.Projection );
			shader.SetUniform( "modelViewProjection", modelView * this.Projection );
			shader.SetUniform( "normalTransform",     Matrix4.Transpose( Matrix4.Invert( modelView ) ) );
			shader.SetUniform( "uvTransform",         this.UV );
			shader.SetUniform( "depthVP",             this.DepthVP );

			this.NeedsReUpload = false;
		}

		#endregion General

		#region Projection

		public void SetProjection( float aspect, float fov, float near, float far )
		{
			this.Projection    = Matrix4.CreatePerspectiveFieldOfView( fov, aspect, near, far );
			this.NeedsReUpload = true;
		}

		public void SetOrthographic( float left, float right, float bottom, float top, float near, float far )
		{
			this.Projection    = Matrix4.CreateOrthographicOffCenter( left, right, bottom, top, near, far );
			this.NeedsReUpload = true;
		}

		#endregion Projection

		#region View

		public void SetCamera( Vector3 eye, Vector3 target, Vector3 up )
		{
			this.View          = Matrix4.LookAt( eye, target, up );
			this.NeedsReUpload = true;
		}

		#endregion View

		#region Model

		public void LoadIdentity()
		{
			this.Model         = Matrix4.Identity;
			this.NeedsReUpload = true;
		}

		public void PushMatrix()
		{
			this.modelTransformStack.Push( this.Model );
		}

		public void PopMatrix()
		{
			this.Model         = this.modelTransformStack.Pop();
			this.NeedsReUpload = true;
		}

		public void Translate( Vector3 amount )
		{
			this.Model         *= Matrix4.CreateTranslation( amount );
			this.NeedsReUpload =  true;
		}

		public void Translate( float x, float y, float z )
		{
			this.Model         *= Matrix4.CreateTranslation( x, y, z );
			this.NeedsReUpload =  true;
		}

		public void Rotate( float angle, Vector3 axis )
		{
			this.Model         *= Matrix4.CreateFromAxisAngle( axis, angle );
			this.NeedsReUpload =  true;
		}

		public void Rotate( float angle, float x, float y, float z )
		{
			this.Model         *= Matrix4.CreateFromAxisAngle( new Vector3( x, y, z ), angle );
			this.NeedsReUpload =  true;
		}

		public void Scale( Vector3 scale )
		{
			this.Model         *= Matrix4.CreateScale( scale );
			this.NeedsReUpload =  true;
		}

		public void Scale( float x, float y, float z )
		{
			this.Model         *= Matrix4.CreateScale( x, y, z );
			this.NeedsReUpload =  true;
		}

		#endregion Model

		#region UV

		public void UVLoadIdentity()
		{
			this.UV            = Matrix4.Identity;
			this.NeedsReUpload = true;
		}

		public void UVPushMatrix()
		{
			this.uvTransformStack.Push( this.UV );
		}

		public void UVPopMatrix()
		{
			this.UV            = this.uvTransformStack.Pop();
			this.NeedsReUpload = true;
		}

		public void UVTranslate( Vector2 amount )
		{
			this.UV            *= Matrix4.CreateTranslation( new Vector3( amount.X, amount.Y, 0.0f ) );
			this.NeedsReUpload =  true;
		}

		public void UVTranslate( float x, float y )
		{
			this.UV            *= Matrix4.CreateTranslation( x, y, 0.0f );
			this.NeedsReUpload =  true;
		}

		public void UVScale( Vector2 scale )
		{
			this.UV            *= Matrix4.CreateScale( new Vector3( scale.X, scale.Y, 1.0f ) );
			this.NeedsReUpload =  true;
		}

		public void UVScale( float x, float y )
		{
			this.UV            *= Matrix4.CreateScale( x, y, 1.0f );
			this.NeedsReUpload =  true;
		}

		#endregion UV

		#region Shadow

		public void CopyToDepthVP()
		{
			this.DepthVP       = this.View * this.Projection * ShadowBiasMatrix;
			this.NeedsReUpload = true;
		}

		#endregion Shadow
	}
}