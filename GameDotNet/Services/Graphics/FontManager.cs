﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Xml.Linq;
using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Abstract.Services.Resources;
using GameDotNet.Graphics;
using GameDotNet.Options;
using GameDotNet.Options.Graphics;
using Font = GameDotNet.Graphics.Font;

namespace GameDotNet.Services.Graphics
{
	public class FontManager : IFontManager
	{
		private readonly Dictionary<string, Font> cache;
		private readonly ITextureLoader           textureLoader;
		private readonly TextureLibrary           textureLibrary;
		private readonly FontLoaderOptions        options;
		private readonly IFontFactory             fontFactory;
		private readonly IResourceProvider        resourceProvider;

		public FontManager( FontLoaderOptions options,
							IFontFactory fontFactory,
							IResourceProvider resourceProvider )
		{
			this.cache            = new Dictionary<string, Font>();
			this.textureLoader    = new DefaultTextureLoader( (BaseDirectoryOptions) options, resourceProvider );
			this.textureLibrary   = new TextureLibrary( this.textureLoader );
			this.options          = options;
			this.fontFactory      = fontFactory;
			this.resourceProvider = resourceProvider;
		}

		public void UnloadAll()
		{
			this.cache.Clear();
		}

		public Font Get( string name )
		{
			if ( this.cache.ContainsKey( name ) )
				return this.cache[ name ];

			var fontDirectory = Path.Combine( this.options.BaseDirectory, name );
			var xmlFileName   = Path.Combine( fontDirectory,              "Font.xml" );

			if ( !this.resourceProvider.ResourceExists( xmlFileName ) )
				throw new FileNotFoundException( $"Font resource not found: {name}" );

			using ( var xmlStream = this.resourceProvider.GetResourceStream( xmlFileName ) )
			{
				var document      = XDocument.Load( xmlStream );
				var rootElement   = document.Element( "fontMetrics" );
				var fileAttribute = rootElement?.Attribute( "file" );

				if ( fileAttribute == null )
					throw new FormatException( $"Error loading font \"{name}\": fontMetrics tag is not present or is missing the file attribute" );

				var textureFileName = Path.Combine( name, fileAttribute.Value );
				var texture         = this.textureLibrary.GetTexture( textureFileName );
				var metrics         = new Dictionary<char, Rectangle>();

				foreach ( var element in rootElement.Elements( "character" ) )
				{
					var xElement      = element.Element( "x" );
					var yElement      = element.Element( "y" );
					var widthElement  = element.Element( "width" );
					var heightElement = element.Element( "height" );

					if ( xElement != null && yElement != null && widthElement != null && heightElement != null )
					{
						var keyAttribute = element.Attribute( "key" );

						if ( ushort.TryParse( keyAttribute?.Value, out var charKey ) &&
							 int.TryParse( xElement.Value,      out var x ) &&
							 int.TryParse( yElement.Value,      out var y ) &&
							 int.TryParse( widthElement.Value,  out var w ) &&
							 int.TryParse( heightElement.Value, out var h ) )
						{
							var bounds = new Rectangle( x, y, w, h );

							metrics[ (char) charKey ] = bounds;
						}
					}
				}

				var font = this.fontFactory.Create( metrics, texture );

				this.cache.Add( name, font );
				return font;
			}
		}
	}
}