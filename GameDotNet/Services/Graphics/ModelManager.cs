using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Graphics;
using Ninject;
using OpenTK.Graphics.OpenGL;

namespace GameDotNet.Services.Graphics
{
	public class ModelManager : IModelManager
	{
		private readonly Dictionary<string, Model> cache;

		private readonly IModelLoader objModelLoader;

		public ModelManager( [ Named( "Obj" ) ] IModelLoader objModelLoader )
		{
			this.cache = new Dictionary<string, Model>();

			this.objModelLoader = objModelLoader;
		}

		public void UnloadModel( string path, string groupName = default )
		{
			var modelName = groupName == default ? path : $"{path}.{groupName}";

			if ( this.cache.ContainsKey( modelName ) )
			{
				this.cache[ modelName ].Delete();
				this.cache.Remove( modelName );
			}
		}

		public void UnloadModel( Model model )
		{
			var keysToDelete = this.cache.Keys.Where( k => this.cache[ k ] == model );

			foreach ( var key in keysToDelete )
				this.cache.Remove( key );

			model.Delete();
		}

		public void UnloadAll()
		{
			GL.BindBuffer( BufferTarget.ArrayBuffer,        0 );
			GL.BindBuffer( BufferTarget.ElementArrayBuffer, 0 );

			foreach ( var model in this.cache.Values )
				model.Delete();

			this.cache.Clear();
		}

		public Model Get( string name, string groupName = default )
		{
			var modelName = groupName == default ? name : $"{name}.{groupName}";

			if ( this.cache.ContainsKey( modelName ) )
				return this.cache[ modelName ];

			var pathExtension = Path.GetExtension( name )?.ToLower();
			var loader = pathExtension switch {
							 ".obj" => this.objModelLoader,
							 _ => throw new NotSupportedException( $"The model file type \"{pathExtension}\" is not supported" )
							 };
			var model = loader.Load( name, groupName );

			this.cache[ modelName ] = model;
			return model;
		}
	}
}