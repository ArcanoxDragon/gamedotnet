﻿using System.Collections.Generic;
using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Graphics;

namespace GameDotNet.Services.Graphics
{
	public class TextureLibraryManager : ITextureLibraryManager
	{
		private readonly Dictionary<string, TextureLibrary> cache;

		private readonly ITextureLoader textureLoader;

		public TextureLibraryManager( ITextureLoader textureLoader )
		{
			this.cache         = new Dictionary<string, TextureLibrary>();
			this.textureLoader = textureLoader;
		}

		public void UnloadAll()
		{
			foreach ( var library in this.cache.Values )
				library.UnloadAllTextures();

			this.cache.Clear();
		}

		public TextureLibrary GetLibrary( string path )
		{
			if ( this.cache.TryGetValue( path, out var library ) )
				return library;

			return this.cache[ path ] = new TextureLibrary( this.textureLoader, path );
		}
	}
}