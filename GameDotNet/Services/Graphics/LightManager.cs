﻿using System;
using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Extensions;
using GameDotNet.Graphics;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace GameDotNet.Services.Graphics
{
	public class LightManager : ILightManager
	{
		public const int SizeLightStruct = sizeof( float ) * 16;
		public const int MaxLights       = 128; // Also defined in level.frag!

		private readonly ILightFactory  lightFactory;
		private readonly IShaderManager shaderManager;
		private readonly int            lightBufferId;

		public LightManager( ILightFactory lightFactory, IShaderManager shaderManager )
		{
			this.lightFactory  = lightFactory;
			this.shaderManager = shaderManager;
			this.lightBufferId = GL.GenBuffer();

			GL.BindBuffer( BufferTarget.UniformBuffer, this.lightBufferId );
			GL.BufferData( BufferTarget.UniformBuffer, (IntPtr) ( SizeLightStruct * MaxLights ), (IntPtr) null, BufferUsageHint.StaticDraw );
		}

		public Light CreateLight() => this.lightFactory.Create();

		public Light CreateNullLight() => this.CreateLight().With( l => {
			l.AmbientColor = Color4.Black;
			l.DiffuseColor = Color4.Black;
			l.Position     = new Vector4( 0, 0, 0, 1 );
		} );

		public void ApplyLights( ref Light[] lights )
		{
			GL.BindBuffer( BufferTarget.UniformBuffer, this.lightBufferId );

			for ( var light = 0; light < MaxLights; light++ )
			{
				float[] data;

				if ( light < lights.Length )
				{
					if ( lights[ light ] == null )
						continue;

					data = lights[ light ].ToArray();
				}
				else
				{
					data = this.CreateNullLight().ToArray();
				}

				GL.BufferSubData( BufferTarget.UniformBuffer, (IntPtr) ( light * SizeLightStruct ), (IntPtr) SizeLightStruct, data );
			}

			GL.BindBuffer( BufferTarget.UniformBuffer, 0 );
		}

		public void BindLightBuffer( Shader shader = null )
		{
			shader??=this.shaderManager.Current;

			var blockLoc = GL.GetUniformBlockIndex( shader.ProgramId, "lights" );

			GL.BindBufferBase( BufferRangeTarget.UniformBuffer, blockLoc, this.lightBufferId );
		}
	}
}