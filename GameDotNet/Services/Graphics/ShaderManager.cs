﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using GameDotNet.Abstract.Services;
using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Abstract.Services.Resources;
using GameDotNet.Graphics;
using GameDotNet.Options.Graphics;

namespace GameDotNet.Services.Graphics
{
	public class ShaderManager : IShaderManager
	{
		private readonly Dictionary<(string, string), Shader> cache;

		private readonly ShaderLoaderOptions options;
		private readonly IResourceProvider   resourceProvider;
		private readonly ITransformManager   transformManager;
		private readonly ILogger             logger;

		public ShaderManager( ShaderLoaderOptions options,
							  IResourceProvider resourceProvider,
							  ITransformManager transformManager,
							  ILogger logger )
		{
			this.cache            = new Dictionary<(string, string), Shader>();
			this.options          = options;
			this.resourceProvider = resourceProvider;
			this.transformManager = transformManager;
			this.logger           = logger;
		}

		public Shader Current { get; private set; }

		public void UnloadShader( string vertexName, string fragmentName = default )
		{
			var cacheKey = ( vertexName, fragmentName );

			if ( this.cache.ContainsKey( cacheKey ) )
			{
				this.cache[ cacheKey ].Delete();
				this.cache.Remove( cacheKey );
			}
		}

		public void UnloadShader( Shader shader )
		{
			var keysToDelete = this.cache.Keys.Where( k => this.cache[ k ] == shader );

			foreach ( var key in keysToDelete )
				this.cache.Remove( key );

			shader.Delete();
		}

		public void UnloadAll()
		{
			foreach ( var s in this.cache.Values )
				s.Delete();

			this.cache.Clear();
		}

		public Shader Get( string vertexName, string fragmentName = default )
		{
			var cacheKey = ( vertexName, fragmentName );

			if ( this.cache.ContainsKey( cacheKey ) )
				return this.cache[ cacheKey ];

			fragmentName??=vertexName;

			if ( fragmentName.EndsWith( ".vert" ) )
				fragmentName = fragmentName.Substring( 0, fragmentName.Length - ".vert".Length );

			if ( !vertexName.EndsWith( ".vert" ) )
				vertexName = $"{vertexName}.vert";
			if ( !fragmentName.EndsWith( ".frag" ) )
				fragmentName = $"{fragmentName}.frag";

			string vertexFilename   = Path.Combine( this.options.BaseDirectory, vertexName );
			string fragmentFilename = Path.Combine( this.options.BaseDirectory, fragmentName );
			string vertexSource;
			string fragmentSource;

			using ( var vertexStream = this.resourceProvider.GetResourceStream( vertexFilename ) )
			using ( var streamReader = new StreamReader( vertexStream ) )
				vertexSource = streamReader.ReadToEnd();

			using ( var fragmentStream = this.resourceProvider.GetResourceStream( fragmentFilename ) )
			using ( var streamReader = new StreamReader( fragmentStream ) )
				fragmentSource = streamReader.ReadToEnd();

			var shader = new Shader( this.transformManager, this.logger, vertexSource, fragmentSource );

			shader.Bound   += this.ShaderBound;
			shader.Unbound += s => this.ShaderUnbound();

			this.cache[ cacheKey ] = shader;
			return shader;
		}

		private void ShaderBound( Shader shader ) => this.Current = shader;
		private void ShaderUnbound() => this.Current = null;
	}
}