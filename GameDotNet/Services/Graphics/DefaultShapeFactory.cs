﻿using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Graphics.Primitives;
using ShapePlane = GameDotNet.Graphics.Primitives.Plane;
using ShapeSphere = GameDotNet.Graphics.Primitives.Sphere;
using ShapeCone = GameDotNet.Graphics.Primitives.Cone;
using ShapeCylinder = GameDotNet.Graphics.Primitives.Cylinder;

namespace GameDotNet.Services.Graphics
{
	public class DefaultShapeFactory : IShapeFactory
	{
		private readonly IModelFactory modelFactory;

		private Shape cachedPlane;
		private Shape cachedSphere;
		private Shape cachedCone;
		private Shape cachedCylinder;

		public DefaultShapeFactory( IModelFactory modelFactory )
		{
			this.modelFactory = modelFactory;
		}

		public Shape Plane    => this.cachedPlane ??= ShapePlane.GenerateShape( this.modelFactory );
		public Shape Sphere   => this.cachedSphere ??= ShapeSphere.GenerateShape( this.modelFactory );
		public Shape Cone     => this.cachedCone ??= ShapeCone.GenerateShape( this.modelFactory );
		public Shape Cylinder => this.cachedCylinder ??= ShapeCylinder.GenerateShape( this.modelFactory );
	}
}