﻿using System;
using System.Collections.Generic;
using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Graphics;

namespace GameDotNet.Services.Graphics
{
	public class AnimationManager : IAnimationManager
	{
		private readonly Dictionary<(int, string), TextureAnimation> cache = new Dictionary<(int, string), TextureAnimation>();

		private readonly ITextureAnimationFactory animationFactory;

		public AnimationManager( ITextureAnimationFactory animationFactory )
		{
			this.animationFactory = animationFactory;
		}

		public void UnloadAll()
		{
			this.cache.Clear();
		}

		public void UpdateAnimations( TimeSpan deltaTime )
		{
			foreach ( var animation in this.cache.Values )
				animation.Update( deltaTime );
		}

		public TextureAnimation GetOrCreate( Texture texture, string name = "" )
		{
			var key = ( texture.TextureId, name );

			if ( !this.cache.ContainsKey( key ) )
				this.cache[ key ] = this.animationFactory.Create( texture );

			return this.cache[ key ];
		}
	}
}