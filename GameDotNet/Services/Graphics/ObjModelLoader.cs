﻿using System;
using System.Collections.Generic;
using System.IO;
using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Abstract.Services.Resources;
using GameDotNet.Extensions.Graphics;
using GameDotNet.Graphics;
using GameDotNet.Options.Graphics;
using ObjLoader.Loader.Data.Elements;
using ObjLoader.Loader.Loaders;
using OpenTK;

namespace GameDotNet.Services.Graphics
{
	public class ObjModelLoader : IModelLoader
	{
		private readonly ModelLoaderOptions      options;
		private readonly IResourceProvider       resourceProvider;
		private readonly IModelFactory           modelFactory;
		private readonly TextureLibrary          textureLibrary;
		private readonly ObjLoaderFactory        loaderFactory;
		private readonly IMaterialStreamProvider materialStreamProvider;

		public ObjModelLoader(
			IResourceProvider resourceProvider,
			IModelFactory modelFactory,
			ITextureLibraryManager textureLibraryManager,
			ModelLoaderOptions options,
			IMaterialStreamProvider materialStreamProvider
		)
		{
			this.options                = options;
			this.resourceProvider       = resourceProvider;
			this.modelFactory           = modelFactory;
			this.textureLibrary         = textureLibraryManager.GetLibrary();
			this.loaderFactory          = new ObjLoaderFactory();
			this.materialStreamProvider = materialStreamProvider;
		}

		public Model Load( string name, string groupName = null )
		{
			var modelName = groupName == default ? name : $"{name}.{groupName}";
			var fileName  = Path.Combine( this.options.BaseDirectory, name );

			if ( !this.resourceProvider.ResourceExists( fileName ) )
				throw new FileNotFoundException( $"Model resource not found: {name}" );

			LoadResult result;

			using ( var modelStream = this.resourceProvider.GetResourceStream( fileName ) )
			{
				var loader = this.loaderFactory.Create( this.materialStreamProvider );

				result = loader.Load( modelStream );
			}

			if ( result.Groups.Count == 0 )
				throw new FileLoadException( $"Model \"{name}\" has no groups!" );

			Group group = null;

			if ( groupName == default )
			{
				group = result.Groups[ 0 ];
			}
			else
			{
				foreach ( var resultGroup in result.Groups )
				{
					if ( resultGroup.Name.ToLower() == groupName.ToLower() )
					{
						group = resultGroup;
						break;
					}
				}

				if ( group == null )
					throw new ArgumentException( $"Model \"{name}\" has no group named \"{groupName}\"!" );
			}

			var  vertices    = new List<Vector4>();
			var  normals     = new List<Vector3>();
			var  uvs         = new List<Vector2>();
			var  indices     = new List<uint>();
			uint vertexIndex = 0;

			foreach ( var face in group.Faces )
			{
				if ( face.Count == 3 ) // Only parse triangular faces!
				{
					for ( var i = 0; i < 3; i++ )
					{
						var vertex = result.Vertices[ face[ i ].VertexIndex - 1 ];
						var normal = result.Normals[ face[ i ].NormalIndex - 1 ];
						var uv     = result.Textures[ face[ i ].TextureIndex - 1 ];

						vertices.Add( new Vector4( vertex.X, vertex.Y, vertex.Z, 1.0f ) );
						normals.Add( new Vector3( normal.X, normal.Y, normal.Z ) );
						uvs.Add( new Vector2( uv.X, uv.Y ) );
						indices.Add( vertexIndex++ );
					}
				}
			}

			var diff  = this.textureLibrary.GetTexture( group.Material.DiffuseTextureMap );
			var norm  = this.textureLibrary.TryGetTexture( group.Material.DiffuseTextureMap + "_n" );
			var spec  = this.textureLibrary.TryGetTexture( group.Material.SpecularTextureMap + "_s" );
			var model = this.modelFactory.Create( modelName );

			model.LoadData( vertices.ToArray(), normals.ToArray(), uvs.ToArray(), indices.ToArray(), diff, norm, spec );

			return model;
		}
	}
}