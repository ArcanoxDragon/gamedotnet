﻿using System.IO;
using GameDotNet.Abstract.Services.Resources;
using GameDotNet.Options.Graphics;
using ObjLoader.Loader.Loaders;

namespace GameDotNet.Services.Graphics
{
	public class DefaultMaterialStreamProvider : IMaterialStreamProvider
	{
		private readonly ModelLoaderOptions options;
		private readonly IResourceProvider  resourceProvider;

		public DefaultMaterialStreamProvider( ModelLoaderOptions options,
											  IResourceProvider resourceProvider )
		{
			this.options          = options;
			this.resourceProvider = resourceProvider;
		}

		public Stream Open( string materialFilePath )
		{
			if ( !materialFilePath.ToLower().EndsWith( ".mtl" ) )
				materialFilePath = $"{materialFilePath}.mtl";

			var fileName = Path.Combine( this.options.MaterialsDirectory, materialFilePath );

			return this.resourceProvider.GetResourceStream( fileName );
		}
	}
}