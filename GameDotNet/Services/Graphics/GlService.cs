﻿using GameDotNet.Abstract.Services.Graphics;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace GameDotNet.Services.Graphics
{
	public class GlService : IGlService
	{
		public GlService( ITransformManager transform,
						  IShapeFactory shapeFactory )
		{
			this.Transform    = transform;
			this.ShapeFactory = shapeFactory;
		}

		public ITransformManager Transform    { get; }
		public IShapeFactory     ShapeFactory { get; }

		public void RenderPlane( int x, int y, int width, int height )
		{
			if ( width == 0 || height == 0 )
				return;

			this.Transform.PushMatrix();
			{
				this.Transform.Scale( width, height, 1 );
				this.Transform.Translate( x, y, 0 );
				this.ShapeFactory.Plane.Render();
			}
			this.Transform.PopMatrix();
		}

		/// <inheritdoc />
		/// <summary>
		/// Uses a texture that defines border and fill to render a tiled plane.
		/// Texture width and height should be divisible by 3.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <param name="borderSize"></param>
		public void RenderBorderedPlane( int x, int y, int width, int height, int borderSize )
		{
			// Don't want glitches
			if ( borderSize <= 0 )
				return;
			if ( width < borderSize * 2 || height < borderSize * 2 )
				return;

			this.Transform.UVPushMatrix();
			{
				this.Transform.UVLoadIdentity();
				this.Transform.UVScale( 1.0f / 3.0f, 1.0f / 3.0f );

				int xTile = ( width - borderSize * 2 ) / borderSize * borderSize; // Get rid of fraction by integer division
				int xRem  = width - borderSize * 2 - xTile;
				int yTile = ( height - borderSize * 2 ) / borderSize * borderSize; // Again, get rid of fraction
				int yRem  = height - borderSize * 2 - yTile;

				#region Middle fill

				this.Transform.UVPushMatrix();
				{
					this.Transform.UVTranslate( 1.0f / 3.0f, 0 );
					for ( int xx = 0; xx < xTile; xx += borderSize )
					{
						this.RenderPlane( x + borderSize + xx, y, borderSize, borderSize );
						this.Transform.UVTranslate( 0, 1.0f / 3.0f );

						// Middle fill
						for ( int yy = 0; yy < yTile; yy += borderSize )
						{
							this.RenderPlane( x + borderSize + xx, y + borderSize + yy, borderSize, borderSize );
						}

						this.Transform.UVPushMatrix();
						{
							this.Transform.UVScale( yRem / (float) borderSize, 1.0f );
							this.RenderPlane( x + borderSize + xx, y + borderSize + yTile, borderSize, yRem );
						}
						this.Transform.UVPopMatrix();

						this.Transform.UVTranslate( 0, 1.0f / 3.0f );
						this.RenderPlane( x + borderSize + xx, y + height - borderSize, borderSize, borderSize );
						this.Transform.UVTranslate( 0, -2.0f / 3.0f );
					}

					this.Transform.UVPushMatrix();
					{
						this.Transform.UVScale( xRem / (float) borderSize, 1.0f );
						this.RenderPlane( x + borderSize + xTile, y, xRem, borderSize );
						this.Transform.UVTranslate( 0, 1.0f / 3.0f );

						// Middle fill
						for ( var yy = 0; yy < yTile; yy += borderSize )
						{
							this.RenderPlane( x + borderSize + xTile, y + borderSize + yy, xRem, borderSize );
						}

						this.Transform.UVPushMatrix();
						{
							this.Transform.UVScale( yRem / (float) borderSize, 1.0f );
							this.RenderPlane( x + borderSize + xTile, y + borderSize + yTile, xRem, yRem );
						}
						this.Transform.UVPopMatrix();

						this.Transform.UVTranslate( 0, 1.0f / 3.0f );
						this.RenderPlane( x + borderSize + xTile, y + height - borderSize, xRem, borderSize );
						this.Transform.UVTranslate( 0, -2.0f / 3.0f );
					}
					this.Transform.UVPopMatrix();
				}
				this.Transform.UVPopMatrix();

				#endregion

				// Top left
				this.RenderPlane( x, y, borderSize, borderSize );
				// Bottom left
				this.Transform.UVTranslate( 0, 2.0f / 3.0f );
				this.RenderPlane( x, y + height - borderSize, borderSize, borderSize );
				// Bottom right
				this.Transform.UVTranslate( 2.0f / 3.0f, 0 );
				this.RenderPlane( x + width - borderSize, y + height - borderSize, borderSize, borderSize );
				// Top right
				this.Transform.UVTranslate( 0, -2.0f / 3.0f );
				this.RenderPlane( x + width - borderSize, y, borderSize, borderSize );

				// Sides fill
				this.Transform.UVTranslate( -2.0f / 3.0f, 1.0f / 3.0f );
				for ( int yy = 0; yy < yTile; yy += borderSize )
				{
					this.RenderPlane( x, y + borderSize + yy, borderSize, borderSize );
					this.Transform.UVTranslate( 2.0f / 3.0f, 0 );
					this.RenderPlane( x + width - borderSize, y + borderSize + yy, borderSize, borderSize );
					this.Transform.UVTranslate( -2.0f / 3.0f, 0 );
				}

				this.Transform.UVPushMatrix();
				{
					this.Transform.UVScale( 1.0f, yRem / (float) borderSize );
					this.RenderPlane( x, y + borderSize + yTile, borderSize, yRem );
					this.Transform.UVTranslate( 2.0f / 3.0f, 0 );
					this.RenderPlane( x + width - borderSize, y + borderSize + yTile, borderSize, yRem );
				}
				this.Transform.UVPopMatrix();
			}
			this.Transform.UVPopMatrix();
		}
	}
}