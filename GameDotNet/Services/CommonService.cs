﻿using GameDotNet.Abstract.Services;
using GameDotNet.Abstract.Services.Audio;
using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Abstract.Services.Resources;

namespace GameDotNet.Services
{
	public class CommonService : ICommonService
	{
		public CommonService(
			ILogger logger,
			ISoundManager sounds,
			ISoundtrackManager soundtracks,
			IAnimationManager animations,
			IFontManager fonts,
			IGlService gl,
			ILightManager lights,
			IModelManager models,
			IShaderManager shaders,
			IShapeFactory shapes,
			ITextureLibraryManager textureLibraries,
			ITransformManager transform,
			IResourceProvider resources
		)
		{
			this.Logger      = logger;
			this.Sounds      = sounds;
			this.Soundtracks = soundtracks;
			this.Animations  = animations;
			this.Fonts       = fonts;
			this.Gl          = gl;
			this.Lights      = lights;
			this.Models      = models;
			this.Shaders     = shaders;
			this.Shapes      = shapes;
			this.Textures    = textureLibraries;
			this.Transform   = transform;
			this.Resources   = resources;
		}

		public ILogger                Logger      { get; }
		public ISoundManager          Sounds      { get; }
		public ISoundtrackManager      Soundtracks { get; }
		public IAnimationManager      Animations  { get; }
		public IFontManager           Fonts       { get; }
		public IGlService             Gl          { get; }
		public ILightManager          Lights      { get; }
		public IModelManager          Models      { get; }
		public IShaderManager         Shaders     { get; }
		public IShapeFactory          Shapes      { get; }
		public ITextureLibraryManager Textures    { get; }
		public ITransformManager      Transform   { get; }
		public IResourceProvider      Resources   { get; }
	}
}