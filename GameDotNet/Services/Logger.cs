﻿using System;
using GameDotNet.Abstract.Services;

namespace GameDotNet.Services
{
	class Logger : ILogger
	{
		private readonly Lazy<ILogWriter> logWriterProvider;

		public Logger( Lazy<ILogWriter> logWriterProvider )
		{
			this.logWriterProvider = logWriterProvider;
		}

		private ILogWriter LogWriter => this.logWriterProvider.Value;

		public void Write( string text )
		{
			this.LogWriter?.Write( text );
		}

		public void Write( string format, params object[] args )
		{
			this.LogWriter?.Write( format, args );
		}

		public void WriteLine( string text )
		{
			this.LogWriter?.WriteLine( text );
		}

		public void WriteLine( string format, params object[] args )
		{
			this.LogWriter?.WriteLine( format, args );
		}
	}
}