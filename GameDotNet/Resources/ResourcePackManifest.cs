﻿namespace GameDotNet.Resources
{
	public class ResourcePackManifest
	{
		public string Id    { get; set; }
		public string Name  { get; set; }
		public int    Layer { get; set; }
	}
}