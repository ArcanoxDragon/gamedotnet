﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using GameDotNet.Utility;

namespace GameDotNet.Resources
{
	public class ResourcePack
	{
		private readonly Dictionary<string, bool> cacheState;

		public ResourcePack( ResourcePackManifest manifest, ZipArchive archive )
		{
			this.cacheState = new Dictionary<string, bool>();

			this.Manifest = manifest;
			this.Archive  = archive;
		}

		public ZipArchive           Archive  { get; }
		public ResourcePackManifest Manifest { get; }

		public string Id    => this.Manifest.Id;
		public string Name  => this.Manifest.Name;
		public int    Layer => this.Manifest.Layer;

		public void UncacheAll()
		{
			foreach ( var fileName in this.cacheState.Keys )
			{
				var cachePath = this.GetCachePath( fileName );

				try
				{
					File.Delete( cachePath );
				}
				catch
				{
					// ignored
				}
			}

			this.cacheState.Clear();
		}

		public bool ContainsFile( string fileName ) => this.Archive.GetEntry( fileName ) != default;

		public Stream OpenFile( string fileName, bool disableCache = false )
		{
			if ( !disableCache && this.cacheState.ContainsKey( fileName ) && this.cacheState[ fileName ] )
				return this.OpenCachedFile( fileName );

			var entry = this.Archive.GetEntry( fileName );

			if ( entry == default )
				throw new FileNotFoundException( $"File \"{fileName}\" not found in resource pack \"{this.Id}\"" );

			if ( disableCache )
				return entry.Open();

			return this.OpenAndCacheEntry( entry, fileName );
		}

		private Stream OpenCachedFile( string fileName )
			=> File.Open( this.GetCachePath( fileName ), FileMode.Open, FileAccess.Read, FileShare.Read );

		private Stream OpenAndCacheEntry( ZipArchiveEntry entry, string fileName )
		{
			var cachePath = this.GetCachePath( fileName );

			entry.ExtractToFile( cachePath, true );

			return this.OpenCachedFile( fileName );
		}

		private string GetCachePath( string fileName )
		{
			var tempFolderPath = FileUtilities.GetTempDirectory();
			var thisPackPath   = Path.Combine( tempFolderPath, "resourcePackCache", this.Id );

			return Path.Combine( thisPackPath, fileName );
		}
	}
}