﻿using System.IO;
using GameDotNet.Graphics;

namespace GameDotNet.Extensions.Graphics
{
	public static class TextureLibraryExtensions
	{
		public static Texture TryGetTexture( this TextureLibrary textureLibrary, string name )
		{
			try
			{
				return textureLibrary.GetTexture( name );
			}
			catch ( FileNotFoundException )
			{
				return null;
			}
		}
	}
}