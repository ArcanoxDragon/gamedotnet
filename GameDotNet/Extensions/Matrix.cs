﻿using OpenTK;

namespace GameDotNet.Extensions
{
	public static class MatrixExtensions
	{
		public static Matrix4 Translate( this Matrix4 matrix, float x, float y, float z )
			=> matrix * Matrix4.CreateTranslation( x, y, z );

		public static Matrix4 Scale( this Matrix4 matrix, float x, float y, float z )
			=> matrix * Matrix4.CreateScale( x, y, z );
	}
}