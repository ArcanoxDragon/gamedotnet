﻿using System.IO;
using System.IO.Compression;
using System.Text;

namespace GameDotNet.Extensions
{
	public static class CompressionExtensions
	{
		public static string ReadAllText( this ZipArchiveEntry entry, Encoding encoding = default )
		{
			using ( var stream = entry.Open() )
			using ( var reader = new StreamReader( stream, encoding ?? Encoding.Default ) )
			{
				return reader.ReadToEnd();
			}
		}
	}
}