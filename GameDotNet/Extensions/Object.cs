﻿using System;

namespace GameDotNet.Extensions
{
	public static class ObjectExtensions
	{
		public static T With<T>( this T obj, Action<T> action )
		{
			action( obj );
			return obj;
		}
	}
}