﻿using OpenTK.Graphics;

namespace GameDotNet.Extensions
{
	public static class ColorExtensions
	{
		/// <summary>
		/// Multiplies this color's intensity by the specified amount
		/// </summary>
		/// <param name="color"></param>
		/// <param name="amount"></param>
		/// <returns></returns>
		public static Color4 Multiply( this Color4 color, float amount )
		{
			return new Color4( color.R * amount, color.G * amount, color.B * amount, color.A );
		}
	}
}