﻿using System;
using GameDotNet.Options;
using GameDotNet.Options.Audio;
using GameDotNet.Options.Graphics;
using GameDotNet.Options.Resources;
using Ninject.Modules;

namespace GameDotNet.Extensions
{
	public static class IoCExtensions
	{
		public static void ConfigureAllOptions( this NinjectModule module, Action<AllGameOptions> configureAction )
		{
			var allOptions = new AllGameOptions();

			configureAction( allOptions );

			module.Bind<SoundLoaderOptions>()
				  .ToConstant( allOptions.Sound );
			module.Bind<SoundtrackLoaderOptions>()
				  .ToConstant( allOptions.Soundtrack );
			module.Bind<FontLoaderOptions>()
				  .ToConstant( allOptions.Font );
			module.Bind<ModelLoaderOptions>()
				  .ToConstant( allOptions.Model );
			module.Bind<ShaderLoaderOptions>()
				  .ToConstant( allOptions.Shader );
			module.Bind<TextureLoaderOptions>()
				  .ToConstant( allOptions.Texture );
			module.Bind<FileSystemResourceLoaderOptions>()
				  .ToConstant( allOptions.FileSystemResource );
			module.Bind<ResourcePackOptions>()
				  .ToConstant( allOptions.ResourcePack );
			module.Bind<GameOptions>()
				  .ToConstant( allOptions.Game );
		}
	}
}