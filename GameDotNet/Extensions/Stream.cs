﻿using System.IO;

namespace GameDotNet.Extensions
{
	public static class StreamExtensions
	{
		/// <summary>
		/// Returns the number of bytes available to be read in the stream
		/// </summary>
		/// <param name="str">Extension method "this" instance</param>
		/// <returns>Number of bytes left to be read in stream</returns>
		public static long GetAvailable( this Stream str )
		{
			return str.Length - str.Position;
		}
	}
}