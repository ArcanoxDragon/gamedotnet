﻿using System.Collections.Generic;
using System.Linq;

namespace GameDotNet.Extensions
{
	public static class LinqExtensions
	{
		/// <summary>
		/// Returns part of an array
		/// </summary>
		/// <typeparam name="T">Type of object in the array</typeparam>
		/// <param name="enumerable">"this" instance of array from which to take objects</param>
		/// <param name="start">Index at which to start taking items</param>
		/// <param name="count">Number of items to take</param>
		/// <returns>Sub-array from the bounds specified</returns>
		public static T[] SubArray<T>( this IEnumerable<T> enumerable, int start, int count )
			=> enumerable.Skip( start ).Take( count ).ToArray();

		public static int AddAndReturnIndex<T>( this ICollection<T> list, T item )
		{
			int index = list.Count;
			list.Add( item );
			return index;
		}
	}
}