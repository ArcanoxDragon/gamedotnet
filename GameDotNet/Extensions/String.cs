﻿using JetBrains.Annotations;

namespace GameDotNet.Extensions
{
	public static class StringExtensions
	{
		/// <summary>
		/// Repeats a string a certain number of times and returns it
		/// </summary>
		/// <param name="str">"this" instance of string to repeat</param>
		/// <param name="times">Number of times to repeat string</param>
		/// <returns>The current string repeated the given number of times</returns>
		public static string Repeat( this string str, int times )
		{
			string ret = "";
			for ( int i = 0; i < times; i++ )
				ret += str;
			return ret;
		}

		[ ContractAnnotation( "str:null => true" ) ]
		public static bool IsNullOrEmpty( this string str ) => string.IsNullOrEmpty( str );

		[ ContractAnnotation( "str:null => true" ) ]
		public static bool IsNullOrWhiteSpace( this string str ) => string.IsNullOrWhiteSpace( str );
	}
}