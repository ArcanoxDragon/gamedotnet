﻿using System.IO;

namespace GameDotNet.Exceptions.Audio
{
	public sealed class WaveLoadException : FileLoadException
	{
		public WaveLoadException( string message ) : base( message ) { }
	}
}