﻿using System.IO;

namespace GameDotNet.Exceptions.Audio
{
	class SoundtrackLoadException : FileLoadException
	{
		public SoundtrackLoadException( string message ) : base( message ) { }
	}
}