﻿using System;
using System.Runtime.Serialization;
using JetBrains.Annotations;

namespace GameDotNet.Exceptions.Graphics
{
	public class ShaderCompileException : Exception
	{
		public ShaderCompileException() { }
		protected ShaderCompileException( [ NotNull ] SerializationInfo info, StreamingContext context ) : base( info, context ) { }
		public ShaderCompileException( string message ) : base( message ) { }
		public ShaderCompileException( string message, Exception innerException ) : base( message, innerException ) { }
	}
}